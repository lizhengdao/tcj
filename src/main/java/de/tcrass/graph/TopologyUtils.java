package de.tcrass.graph;

import java.util.Comparator;

import de.tcrass.math.data.Data;

/**
 * A class providing methods for simple topological analyses on
 * {@link Graph}s.
 */
public class TopologyUtils {

    /**
     * The default key under which topolical numbers will be stored in
     * the
     * 
     * {@link Node}s' {@link de.tcrass.util.DataStore}s.
     */
    public static final String DEFAULT_DATA_KEY_TOPOLOGICAL_NUMBER = "__TOPOLOGICAL_NUMBERING__";

    // --- Private methods ---

    /*
     * Recursively assigns topological numbers to the given graph's
     * nodes.
     */
    private static long assignTopologicalNumbering(Graph g, Node node, long n, String topoNumKey)
    throws GraphException {

        long cn = n;
        node.data().set(topoNumKey, -1l);
        for (Node successor : node.getSuccessors()) {
            Long sn = successor.data().getLong(topoNumKey);
            if (sn < 0) {
                throw new GraphException(g,
                    "Cannot complete topological numbering since graph " + g.getIdString() +
                    " is cyclic! (Node " + successor.getIdString() + " revisited)");
            }
            else if (sn == 0) {
                cn = assignTopologicalNumbering(g, successor, cn, topoNumKey);
            }
        }
        node.data().set(topoNumKey, cn);
        return cn - 1;
    }

    // --- Public methods ---

    // --- Degree distribution

    /**
     * Returns a {@link de.tcrass.math.data.Data} table containing three
     * columns (named <code>in</code>, <code>out</code> and
     * <code>inout</code>) containing the frequency with which the in-,
     * out, and in/out-degree value corresponding to the row index
     * occurs within the set of all the {@link Graph}'s {@link Node}s.
     */
    public static Data getDegreeDistribution(Graph g) {
        Data d = Data.intData("in", "out", "inout");
        for (Node n : g.getNodes()) {
            int in = n.getInEdges().size();
            int out = n.getOutEdges().size();
            d.setInt("in", in, d.getInt("in", in) + 1);
            d.setInt("out", out, d.getInt("out", out) + 1);
            d.setInt("inout", in + out, d.getInt("inout", in + out) + 1);
        }
        return d;
    }

    // --- Topological sorting/numbering

    /**
     * Creates a {@link java.util.Comparator} comparing two {@link Node}
     * s with respect to their topological numbering which is expected
     * to be stored in their {@link de.tcrass.util.DataStore}s under the
     * given key.
     * 
     * @param topoNumKey
     *            the key under which the topological numbering is
     *            stored within the nodes' DataStores
     * @return a Comparator comparing nodes with respect to thier
     *         topological numbering
     */
    public static Comparator<Node> createTopologicalNumberingComparator(String topoNumKey) {
        final String key = topoNumKey;
        Comparator<Node> comp = new Comparator<Node>() {

            public int compare(Node n1, Node n2) {
                long d1 = n1.data().getLong(key);
                long d2 = n2.data().getLong(key);
                return (d1 < d2 ? -1 : (d1 > d2 ? 1 : 0));
            }
        };
        return comp;
    }

    /**
     * Calculates topological numbers for each of the given directed
     * {@link Graph}'s {@link Node}s and stores them in the node's
     * {@link de.tcrass.util.DataStore}s under the given key.
     * <p>
     * Topological numbering assigns integer values <i>t</i> to nodes so
     * that <i>t</i>(node 1) &lt; <i>t</i>(node 2) if there exists a
     * path from node 1 to node 2. This procedure is not applicable to
     * graphs containing cycles; consequently, if a cycle is detected,
     * the numbering process ist aborted and an exception gets thrown.
     * 
     * @param g
     *            the graph to which to assign topolgical numbering
     * @param topoNumKey
     *            the key under which the calculated topological numbers
     *            will be stored in the nodes' DataStores
     * @throws GraphException
     */
    public static void assignTopologicalNumbering(Graph g, String topoNumKey)
    throws GraphException {
        long n = g.getNodes().size();
        GraphUtils.setAllNodeData(g, topoNumKey, 0l);
        for (Node node : g.getNodes()) {
            if (node.data().getLong(topoNumKey) == 0) {
                n = assignTopologicalNumbering(g, node, n, topoNumKey);
            }
        }
    }

    /**
     * Calculates topological numbers for each of the given directed
     * {@link Graph}'s {@link Node}s and stores them in the node's
     * {@link de.tcrass.util.DataStore}s under the
     * <code>DEFAULT_DATA_KEY_TOPOLOGICAL_NUMBER</code> key.
     * <p>
     * Topological numbering assigns integer values <i>t</i> to nodes so
     * that <i>t</i>(node 1) &lt; <i>t</i>(node 2) if there exists a
     * path from node 1 to node 2. This procedure is not applicable to
     * graphs containing cycles; consequently, if a cycle is detected,
     * the numbering process ist aborted and an exception gets thrown.
     * 
     * @param g
     *            the graph to which to assign topolgical numbering
     * @throws GraphException
     */
    public static void assignTopologicalNumbering(Graph g) throws GraphException {
        assignTopologicalNumbering(g, DEFAULT_DATA_KEY_TOPOLOGICAL_NUMBER);
    }

}
