package de.tcrass.graph;

import java.util.Properties;

import de.tcrass.util.DataStore;

/**
 * The abstract base class of things actually making up a
 * 
 * {@link Graph}, i.&nbsp;e.&nbsp;its {@link Node}s and {@link Edge}s.
 * <p>
 * Many graph algorithms require nodes and/or edges to be marked, or
 * labelled, with either an integer value, frequently called "coloring",
 * or a real number, usually called "weight". For convenience, the
 * GraphElement class defines accordingly named accessor methods for an
 * {@link int} and a {@link double} value which get stored in the
 * GraphElement's {@link DataStore}.
 */
public abstract class GraphElement extends GraphObject implements Cloneable {

    /**
     * Reserved key for storing the GraphElement's coloring in its
     * {@link DataStore}.
     */
    public static final String DATA_KEY_COLORING = "__COLORING__";

    /**
     * Reserved key for storing the GraphElement's weight in its
     * DataStore.
     */
    public static final String DATA_KEY_WEIGHT   = "__WEIGHT__";

    // --- Constructors ---

    /**
     * Creates a new GraphElement instance with the given id and the
     * given Properties as Graphviz attributes (if not null).
     * 
     * @param id
     *            the object to be used as the new GraphElement's id
     *            (should be either an Integer or a String}
     * @param p
     *            the Graphviz attributes to be assigned to the new
     *            GraphElement (may be null)
     */
    protected GraphElement(Object id, Properties p) {
        super(id, p);
    }

    // --- Public methods

    /**
     * Sets this GraphElement's "coloring" (i.e. an arbitrary int
     * value).
     * 
     * @param c
     *            the GraphElement's new coloring
     */
    public void setColoring(int c) {
        data().set(DATA_KEY_COLORING, c);
    }

    /**
     * Returns the GraphElement's "coloring".
     * 
     * @return the coloring
     */
    public int getColoring() {
        return data().getInt(DATA_KEY_COLORING);
    }

    /**
     * Sets this GraphElement's "weight" (i.e. an arbitrary double
     * value).
     * 
     * @param w
     *            the GraphElement's new weight
     */
    public void setWeight(double w) {
        data().set(DATA_KEY_WEIGHT, w);
    }

    /**
     * Returns the GraphElement's "weight"
     * 
     * @return the weight
     */
    public double getWeight() {
        return data().getDouble(DATA_KEY_WEIGHT);
    }

}
