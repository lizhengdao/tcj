//package de.tcrass.graph.jung;
//
//import java.util.Iterator;
//
//import de.tcrass.graph.GraphException;
//import de.tcrass.graph.GraphObject;
//import de.tcrass.graph.Node;
//import de.tcrass.util.BidiMap;
//import edu.uci.ics.jung.graph.Vertex;
//
//public class JungConnector {
//
//    private static void copyUserDataFromJung(edu.uci.ics.jung.utils.UserDataContainer j,
//                                             GraphObject o) {
//        for (Iterator<?> i = j.getUserDatumKeyIterator(); i.hasNext();) {
//            Object key = i.next();
//            o.data().set(key.toString(), j.getUserDatum(key));
//            // System.out.println("Mapping key " + key + ": " +
//            // j.getUserDatum(key) + " => " +
//            // o.getDataStore().getData(key.toString()));
//        }
//    }
//
//    public static de.tcrass.graph.Graph toGraph(edu.uci.ics.jung.graph.ArchetypeGraph jg,
//                                                String idKey) {
//
//        boolean directed = true;
//
//        de.tcrass.graph.Graph g = new de.tcrass.graph.Graph();
//
//        BidiMap nodes2jung = new BidiMap();
//
//        for (Object o : jg.getVertices()) {
//            Vertex jn = (Vertex) o;
//            if (jn.containsUserDatumKey(idKey)) {
//                Object id = jn.getUserDatum(idKey);
//                Node n;
//                if (id instanceof Integer) {
//                    n = g.createNode((Integer) jn.getUserDatum(idKey));
//                }
//                else {
//                    n = g.createNode(jn.getUserDatum(idKey).toString());
//                }
//                copyUserDataFromJung(jn, n);
//                nodes2jung.put(n, jn);
//            }
//            else {
//                throw new GraphException("Missing specified id field '" + idKey +
//                                         "' in user data of JUNG Vertex " + jn.toString());
//            }
//        }
//
//        for (Object o : jg.getEdges()) {
//            if (directed) {
//                edu.uci.ics.jung.graph.DirectedEdge je = (edu.uci.ics.jung.graph.DirectedEdge) o;
//                Node tail = (Node) nodes2jung.getBackward(je.getSource());
//                Node head = (Node) nodes2jung.getBackward(je.getDest());
//                de.tcrass.graph.Edge e = g.createEdge(tail, head);
//                copyUserDataFromJung(je, e);
//            }
//        }
//
//        return g;
//    }
//
//}
