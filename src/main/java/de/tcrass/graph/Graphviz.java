package de.tcrass.graph;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.imageio.ImageIO;

import de.tcrass.io.ExternalProcess;
import de.tcrass.io.FileUtils;
import de.tcrass.io.ParseException;
import de.tcrass.util.StringUtils;
import de.tcrass.util.SysUtils;

/**
 * This class provides methods for reading and writing {@link Graph} objects to
 * or from, respectively, AT&T Graphviz .dot files. To this end, it makes use of
 * the {@link SimpleDotParser} implementation.
 * <p>
 * Please note that, due to the fact that dot is a graph drawing language rather
 * than a graph description language, some limitations exist with regard to
 * which properties of a Graph object (and its {@link GraphElement}s) can be
 * written to or read from a .dot file. More details about these semantic
 * aspects can be found in the {@link Graph} and {@link Edge} classes'
 * descriptions.
 */
public class Graphviz {

    // --- Enums

    /**
     * An enumeration designating the different Graphviz layout methods
     */
    public enum LayoutMethod {
        DOT, NEATO, CIRCO, TWOPI, FDP
    };

    // --- Static variables

    /*
     * Command for determining possible output formats
     */
    private static String FORMAT_CMD = "dot -Txxx";
    /*
     * Paths to tools for different layout methods
     */
    private static final Map<LayoutMethod, String> GRAPHVIZ_TOOL_PATHS = new HashMap<LayoutMethod, String>();

    /**
     * A List version of the corresponding Array.
     */
    static List<String> DOT_LABEL_STRING_PROPERTIES_LIST;

    /**
     * An array of Graphviz "lblString" attributes, which are attributes having
     * either plain Strings or Graphviz HTML strings as possible values.
     */
    public static final String[] DOT_LABEL_STRING_PROPERTIES = { "URL",
            "edgeURL", "edgehref", "headURL", "headhref", "headlabel", "href",
            "label", "labelURL", "labelhref", "tailURL", "tailhref",
            "taillabel" };

    /**
     * The list of output formats understood by your local installation of
     * Graphviz.
     */
    public static final String[] OUTPUT_FORMATS;

    /**
     * Constant for referring to the Graphviz dot program.
     */
    @Deprecated
    public static int DOT_LAYOUT = 0;
    /**
     * Constant for referring to the Graphviz neato program.
     */
    @Deprecated
    public static int NEATO_LAYOUT = 1;
    /**
     * Constant for referring to the Graphviz twopi program.
     */
    @Deprecated
    public static int TWOPI_LAYOUT = 2;
    /**
     * Constant for referring to the Graphviz circo program.
     */
    @Deprecated
    public static int CIRCO_LAYOUT = 3;
    /**
     * Constant for referring to the Graphviz fdp program.
     */
    @Deprecated
    public static int FDP_LAYOUT = 4;

    // --- Static constructor ---

    static {
        DOT_LABEL_STRING_PROPERTIES_LIST = new ArrayList<String>(Arrays
                .asList(DOT_LABEL_STRING_PROPERTIES));

        ExternalProcess p = new ExternalProcess(FORMAT_CMD);
        String[] formats;
        try {
            p.launchAndWait();
            String out = StringUtils.chomp(p.getStdErr());
            formats = (out.substring(out.indexOf("one of: ") + 8))
                    .split(" ");
        } catch (IOException e) {
            formats = new String[0];
            throw new GraphException(null,
                    "Graphviz dot program not installed or not in PATH.");
        }
        OUTPUT_FORMATS = formats;

        GRAPHVIZ_TOOL_PATHS.put(LayoutMethod.DOT, "dot");
        GRAPHVIZ_TOOL_PATHS.put(LayoutMethod.NEATO, "neato");
        GRAPHVIZ_TOOL_PATHS.put(LayoutMethod.TWOPI, "twopi");
        GRAPHVIZ_TOOL_PATHS.put(LayoutMethod.CIRCO, "circo");
        GRAPHVIZ_TOOL_PATHS.put(LayoutMethod.FDP, "fdp");

    }

    // --- Attribute access ---

    /**
     * Returns the mapping of layout mehtods ot the corresponding Graphviz
     * tool's path
     */
    public static Map<LayoutMethod, String> getToolPaths() {
        return new HashMap<LayoutMethod, String>(GRAPHVIZ_TOOL_PATHS);
    }

    /**
     * Sets the path for the tool performing the given layou method
     */
    public static void setToolPath(LayoutMethod method, String path) {
        GRAPHVIZ_TOOL_PATHS.put(method, path);
    }

    // --- Private methods ---

    private static String getPropertyString(Properties p) {
        StringBuffer s = new StringBuffer();
        // Properties p = g.getProperties();
        if (p.size() != 0) {
            s.append(" [");
            int i = 0;
            for (Enumeration<Object> e = p.keys(); e.hasMoreElements();) {
                if (i > 0) {
                    s.append(", ");
                }
                String key = (String) e.nextElement();
                if (DOT_LABEL_STRING_PROPERTIES_LIST.contains(key)) {
                    if (p.getProperty(key).startsWith("<")
                            && p.getProperty(key).endsWith(">")) {
                        s.append(key + "=" + p.getProperty(key));
                    } else {
                        s.append(key + "=\"" + escape(p.getProperty(key))
                                + "\"");
                    }
                } else {
                    s.append(key + "=\"" + p.getProperty(key) + "\"");
                }
                i++;
            }
            s.append("]");
        }
        return s.toString();
    }

    private static void writeStmtString(Graph g, Writer w, int indent)
            throws IOException {
        String indStr = StringUtils.polymerize("  ", indent);
        w.write("{");
        if (g.getGVAttributes().size() > 0) {
            w.write(" graph " + getPropertyString(g.getGVAttributes()));
        }
        w.write("\n");

        if (g.getDefaultGVNodeAttributes().size() > 0) {
            w.write(indStr + "  node"
                    + getPropertyString(g.getDefaultGVNodeAttributes()) + "\n");
        }

        if (g.getDefaultGVEdgeAttributes().size() > 0) {
            w.write(indStr + "  edge"
                    + getPropertyString(g.getDefaultGVEdgeAttributes()) + "\n");
        }

        for (Node n : g.getDirectlyOwnedNodes()) {
            w.write(indStr + "  \"" + escape(n.getIdString()) + "\"");
            w.write(getPropertyString(n.getGVAttributes()));
            w.write(";\n");
        }

        for (Graph sg : g.getDirectlyOwnedSubgraphs()) {
            w.write(indStr + "  ");
            if (sg.getIdString() != null) {
                w.write("subgraph ");
                if (sg.getId() != null) {
                    w.write("\"" + escape(sg.getIdString()) + "\" ");
                }
            }
            writeStmtString(sg, w, indent + 1);
        }

        String edgeType = (g.isDirected() ? " -> " : " -- ");
        for (Edge e : g.getDirectlyOwnedEdges()) {
            w.write(indStr + "  \"" + escape(e.getTail().getIdString()) + "\"");
            if (e.getTailPort() != null) {
                w.write(":\"" + escape(e.getTailPort()) + "\"");
            }
            w.write(edgeType);
            w.write("\"" + escape(e.getHead().getIdString()) + "\"");
            if (e.getHeadPort() != null) {
                w.write(":\"" + escape(e.getHeadPort()) + "\"");
            }
            w.write(getPropertyString(e.getGVAttributes()));
            w.write(";\n");
        }

        w.write(indStr + "}\n");
    }

    // --- Public methods ---

    /**
     * Escapes within the given String some characters according to Graphviz'
     * needs. In particular, newline, carriage return, tab, backslash and single
     * quote will be substituted with <code>\n</code>, <code>\r</code>,
     * <code>\t</code>, <code>\\</code> and <code>\'</code>, respectively.
     * 
     * @param str
     *            the String to be escaped
     * @return an escaped version of the String
     */
    public static String escape(String str) {
        str = StringUtils.replaceAllMatches(str, "\\\\([^lnrEGHT])", "\\\\$1");
        str = StringUtils.replaceAll(str, "\"", "\\\"");
        str = StringUtils.replaceAll(str, "\n", "\\n");
        str = StringUtils.replaceAll(str, "\r", "\\r");
        str = StringUtils.replaceAll(str, "\t", "\\t");
        return str;
    }

    /**
     * Parses the given String as dot language-based graph specification and
     * returns a corresponding {@link Graph} object.
     * 
     * @param s
     *            the String to be parsed
     * @return a graph compliant with the given dot string
     * @throws ParseException
     */
    public static Graph parseSimpleDotString(String s) throws ParseException {
        Graph g = null;
        StringReader sr = new StringReader(s);
        try {
            SimpleDotParser p = new SimpleDotParser(sr);
            g = p.parse();
        } catch (IOException e) {
            SysUtils.reportException(e);
        }
        sr.close();
        return g;
    }

    /**
     * Reads the text file specified by the given path, parses it according to
     * dot language specifications and returns a corresponding Graph object.
     * 
     * @param path
     *            the path to the dot file
     * @return a Graph corrsponding to the description stored in the dot file
     * @throws IOException
     * @throws ParseException
     */
    public static Graph readSimpleDotFile(String path) throws IOException,
            ParseException {
        return readSimpleDotFile(new File(path));
    }

    /**
     * Parses the contents of the specified text File according to dot language
     * specifications and returns a corresponding Graph object.
     * 
     * @param f
     *            the dot file
     * @return a Graph corrsponding to the description stored in the dot file
     * @throws IOException
     * @throws ParseException
     */
    public static Graph readSimpleDotFile(File f) throws IOException,
            ParseException {
        Graph g;
        FileReader fr = new FileReader(f);
        SimpleDotParser p = new SimpleDotParser(fr);
        g = p.parse();
        fr.close();
        return g;
    }

    /**
     * Creates from the given Graph a String representing a description thereof
     * in dot language.
     * 
     * @param g
     *            the Graph a dot language description is desired for
     * @return the dot language description of the given Graph
     */
    public static String createDotString(Graph g) {
        String dot = "";
        StringWriter w = new StringWriter();
        try {
            writeDotString(g, w);
            dot = w.getBuffer().toString();
        } catch (IOException e) {
            SysUtils.reportException(e);
        }
        return dot;
    }

    /**
     * Creates from the given Graph a String representing a description thereof
     * in dot language and writes this String using the given Writer.
     * 
     * @param g
     *            the Graph a dot language description of which is to be written
     * @param w
     *            the Writer to be used for writing
     * @throws IOException
     */
    public static void writeDotString(Graph g, Writer w) throws IOException {
        w.write(g.isDirected() ? "digraph" : "graph");
        if (g.getIdString() != null) {
            w.write(" \"" + escape(g.getIdString()) + "\"");
        }
        w.write(" ");
        writeStmtString(g, w, 0);
        w.flush();
    }

    /**
     * Creates from the given Graph a String representing a description thereof
     * in dot language and writes this String to the file spcified by the given
     * path.
     * 
     * @param g
     *            the Graph a dot language description of which is to be written
     * @param path
     *            the path to the dot file to be written to
     */
    public static void writeDotFile(Graph g, String path) throws IOException {
        writeDotFile(g, new File(path));
    }

    /**
     * Creates from the given Graph a String representing a description thereof
     * in dot language and writes this String to the given File.
     * 
     * @param g
     *            the Graph a dot language description of which is to be written
     * @param f
     *            the path to the dot file to be written to
     */
    public static void writeDotFile(Graph g, File f) throws IOException {
        FileWriter w = new FileWriter(f);
        writeDotString(g, w);
        w.flush();
        w.close();
    }

    /**
     * Creates a clone of the given Graph which will contain additional layout
     * information, gained using one of the Graphviz layout programs.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     */
    public static Graph doLayoutToGraph(Graph g, LayoutMethod method)
            throws IOException {
        return doLayoutToGraph(g, method, new String[0]);
    }

    /**
     * Creates a clone of the given Graph which will contain additional layout
     * information, gained using one of the Graphviz layout programs.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     */
    @Deprecated
    public static Graph doLayoutToGraph(Graph g, int method) throws IOException {
        return doLayoutToGraph(g, LayoutMethod.values()[method], new String[0]);
    }

    /**
     * Creates a clone of the given Graph which will contain additional layout
     * information, gained using one of the Graphviz layout programs.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     * @param args
     *            a (possibly empty) String Array of additional command-line
     *            arguments for the chosen Graphviz layout programm
     */
    public static Graph doLayoutToGraph(Graph g, LayoutMethod method,
            String[] args) throws IOException {
        String[] cmd = new String[args.length + 1];
        cmd[0] = GRAPHVIZ_TOOL_PATHS.get(method);
        for (int i = 0; i < args.length; i++) {
            cmd[1 + i] = args[i];
        }

        String cmdStr = "";
        for (String element : cmd) {
            cmdStr = cmdStr + element + " ";
        }

        ExternalProcess p = new ExternalProcess(cmdStr);
        p.launch();
        writeDotString(g, p.getStdInWriter());
        p.waitFor();

        Graph result = null;
        try {
            result = parseSimpleDotString(p.getStdOut());
        } catch (ParseException e) {
            SysUtils.reportException(e);
        }

        return result;
    }

    /**
     * Creates String containing the output obtained from layouting the given
     * Graph using one of the Graphviz layout programs and an arbitrary output
     * format.
     * <p>
     * Use <code>DOT_OUTPUT</code> for a dot language representation containing
     * additional layout information and something like <code>PS_OUTPUT</code>
     * or <code>IMAP_OUTPUT</code> for other text-based output formats. When
     * using a binary output format, such as <code>PNG_OUTPUT</code>, the
     * low-bytes of the String's 16-bit characters will contain the actual data.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     * @param format
     *            a valid output format according to
     *            {@link Graphviz#OUTPUT_FORMATS}
     * @return the layout result
     */
    public static String doLayoutToString(Graph g, LayoutMethod method,
            String format) throws IOException {
        return doLayoutToString(g, method, format, new String[0]);
    }

    /**
     * Creates String containing the output obtained from layouting the given
     * Graph using one of the Graphviz layout programs and an arbitrary output
     * format.
     * <p>
     * Use <code>DOT_OUTPUT</code> for a dot language representation containing
     * additional layout information and something like <code>PS_OUTPUT</code>
     * or <code>IMAP_OUTPUT</code> for other text-based output formats. When
     * using a binary output format, such as <code>PNG_OUTPUT</code>, the
     * low-bytes of the String's 16-bit characters will contain the actual data.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     * @param format
     *            a valid output format according to
     *            {@link Graphviz#OUTPUT_FORMATS}
     * @return the layout result
     */
    @Deprecated
    public static String doLayoutToString(Graph g, int method, String format)
            throws IOException {
        return doLayoutToString(g, LayoutMethod.values()[method], format,
                new String[0]);
    }

    /**
     * Creates String containing the output obtained from layouting the given
     * Graph using one of the Graphviz layout programs and an arbitrary output
     * format.
     * <p>
     * Use <code>DOT_OUTPUT</code> for a dot language representation containing
     * additional layout information and something like <code>PS_OUTPUT</code>
     * or <code>IMAP_OUTPUT</code> for other text-based output formats. When
     * using a binary output format, such as <code>PNG_OUTPUT</code>, the
     * low-bytes of the String's 16-bit characters will contain the actual data.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     * @param format
     *            a valid output format according to
     *            {@link Graphviz#OUTPUT_FORMATS}
     * @param args
     *            a (possibly empty) String Array of additional command-line
     *            arguments for the chosen Graphviz layout programm
     * @return the layout result
     */
    public static String doLayoutToString(Graph g, LayoutMethod method,
            String format, String[] args) throws IOException {
        String[] cmd = new String[args.length + 2];
        cmd[0] = GRAPHVIZ_TOOL_PATHS.get(method);
        cmd[1] = "-T" + format;
        for (int i = 0; i < args.length; i++) {
            cmd[2 + i] = args[i];
        }

        String cmdStr = "";
        for (String element : cmd) {
            cmdStr = cmdStr + element + " ";
        }

        ExternalProcess p = new ExternalProcess(cmdStr);
        p.launch();
        writeDotString(g, p.getStdInWriter());
        p.waitFor();

        return p.getStdOut();
    }

    /**
     * Writes the output obtained from layouting the given Graph using one of
     * the Graphviz layout programs and an arbitrary output format to the file
     * specified by the given path.
     * <p>
     * In order to be sure the layout process is finished when you need its
     * result, use {@link de.tcrass.io.ExternalProcess#waitFor()} on this
     * method's return value.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     * @param format
     *            a valid output format according to
     *            {@link Graphviz#OUTPUT_FORMATS}
     * @return the {@link ExternalProcess} of the running layout program
     */
    public static ExternalProcess doLayoutToFile(Graph g, String path,
            LayoutMethod method, String format) throws IOException {
        return doLayoutToFile(g, path, method, format, new String[0]);
    }

    /**
     * Writes the output obtained from layouting the given Graph using one of
     * the Graphviz layout programs and an arbitrary output format to the file
     * specified by the given path.
     * <p>
     * In order to be sure the layout process is finished when you need its
     * result, use {@link de.tcrass.io.ExternalProcess#waitFor()} on this
     * method's return value.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     * @param format
     *            a valid output format according to
     *            {@link Graphviz#OUTPUT_FORMATS}
     * @return the {@link ExternalProcess} of the running layout program
     */
    @Deprecated
    public static ExternalProcess doLayoutToFile(Graph g, String path,
            int method, String format) throws IOException {
        return doLayoutToFile(g, path, LayoutMethod.values()[method], format,
                new String[0]);
    }

    /**
     * Writes the output obtained from layouting the given Graph using one of
     * the Graphviz layout programs and an arbitrary output format to the file
     * specified by the given path.
     * <p>
     * In order to be sure the layout process is finished when you need its
     * result, use {@link de.tcrass.io.ExternalProcess#waitFor()} on this
     * method's return value.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     * @param format
     *            a valid output format according to
     *            {@link Graphviz#OUTPUT_FORMATS}
     * @param args
     *            a (possibly empty) String Array of additional command-line
     *            arguments for the chosen Graphviz layout programm
     * @return the {@link ExternalProcess} of the running layout program
     */
    public static ExternalProcess doLayoutToFile(Graph g, String path,
            LayoutMethod method, String format, String[] args)
            throws IOException {
        return doLayoutToFile(g, new File(path), method, format, args);
    }

    /**
     * Writes the output obtained from layouting the given Graph using one of
     * the Graphviz layout programs and an arbitrary output format to the given
     * File.
     * <p>
     * In order to be sure the layout process is finished when you need its
     * result, use {@link de.tcrass.io.ExternalProcess#waitFor()} on this
     * method's return value.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     * @param format
     *            a valid output format according to
     *            {@link Graphviz#OUTPUT_FORMATS}
     * @return the {@link ExternalProcess} of the running layout program
     */
    public static ExternalProcess doLayoutToFile(Graph g, File target,
            LayoutMethod method, String format) throws IOException {
        return doLayoutToFile(g, target, method, format, new String[0]);
    }

    /**
     * Writes the output obtained from layouting the given Graph using one of
     * the Graphviz layout programs and an arbitrary output format to the given
     * File.
     * <p>
     * In order to be sure the layout process is finished when you need its
     * result, use {@link de.tcrass.io.ExternalProcess#waitFor()} on this
     * method's return value.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     * @param format
     *            a valid output format according to
     *            {@link Graphviz#OUTPUT_FORMATS}
     * @return the {@link ExternalProcess} of the running layout program
     */
    @Deprecated
    public static ExternalProcess doLayoutToFile(Graph g, File target,
            int method, String format) throws IOException {
        return doLayoutToFile(g, target, LayoutMethod.values()[method], format,
                new String[0]);
    }

    /**
     * Writes the output obtained from layouting the given Graph using one of
     * the Graphviz layout programs and an arbitrary output format to the given
     * File.
     * <p>
     * In order to be sure the layout process is finished when you need its
     * result, use {@link de.tcrass.io.ExternalProcess#waitFor()} on this
     * method's return value.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     * @param format
     *            a valid output format according to
     *            {@link Graphviz#OUTPUT_FORMATS}
     * @param args
     *            a (possibly empty) String Array of additional command-line
     *            arguments for the chosen Graphviz layout programm
     * @return the {@link ExternalProcess} of the running layout program
     */
    public static ExternalProcess doLayoutToFile(Graph g, File target,
            LayoutMethod method, String format, String[] args)
            throws IOException {
        String[] cmd = new String[args.length + 4];
        cmd[0] = GRAPHVIZ_TOOL_PATHS.get(method);
        cmd[1] = "-T" + format;
        cmd[2] = "-o ";
        cmd[3] = FileUtils.escapeFilename(target.getCanonicalPath());
        for (int i = 0; i < args.length; i++) {
            cmd[4 + i] = args[i];
        }

        String cmdStr = "";
        for (String element : cmd) {
            cmdStr = cmdStr + element + " ";
        }

        ExternalProcess p = new ExternalProcess(cmdStr);
        p.launch();
        writeDotString(g, p.getStdInWriter());
        p.waitFor();

        return p;
    }

    /**
     * Returns a {@link BufferedImage} containing the result obtained from
     * layouting the given Graph using one of the Graphviz layout programs and
     * an arbitrary output format.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     * @return the {@link ExternalProcess} of the running layout program
     */
    public static BufferedImage doLayoutToImage(Graph g, LayoutMethod method)
            throws IOException {
        return doLayoutToImage(g, method, new String[0]);
    }

    /**
     * Returns a {@link BufferedImage} containing the result obtained from
     * layouting the given Graph using one of the Graphviz layout programs and
     * an arbitrary output format.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     * @return the {@link ExternalProcess} of the running layout program
     */
    @Deprecated
    public static BufferedImage doLayoutToImage(Graph g, int method)
            throws IOException {
        return doLayoutToImage(g, LayoutMethod.values()[method], new String[0]);
    }

    /**
     * Returns a {@link BufferedImage} containing the result obtained from
     * layouting the given Graph using one of the Graphviz layout programs and
     * an arbitrary output format.
     * 
     * @param g
     *            the Graph to be layouted
     * @param method
     *            a constant telling which layout method (i.e. which Graphviz
     *            programm) to use
     * @param args
     *            a (possibly empty) String Array of additional command-line
     *            arguments for the chosen Graphviz layout programm
     * @return the {@link ExternalProcess} of the running layout program
     */
    public static BufferedImage doLayoutToImage(Graph g, LayoutMethod method,
            String[] args) throws IOException {
        String[] cmd = new String[args.length + 2];
        cmd[0] = GRAPHVIZ_TOOL_PATHS.get(method);
        cmd[1] = "-Tpng";
        for (int i = 0; i < args.length; i++) {
            cmd[2 + i] = args[i];
        }

        String cmdStr = "";
        for (String element : cmd) {
            cmdStr = cmdStr + element + " ";
        }

        ExternalProcess p = new ExternalProcess(cmdStr);
        p.launch();
        writeDotString(g, p.getStdInWriter());
        p.waitFor();

        byte[] bytes = p.getStdOutRecorder().getByteBuffer();

        InputStream is = new ByteArrayInputStream(bytes);
        BufferedImage im = ImageIO.read(is);

        return im;
    }

}
