package de.tcrass.graph;

/**
 * An interface defining a method for calculating a {@link GraphElement}
 * 's weight. Used e. g. by {@link ShortestPathUtils}.
 */
public interface WeightFunction {

    /**
     * Returns the given GraphElement's weight.
     * 
     * @param o
     *            the GraphElement whose weight is to be determined
     * @return the GraphElement's weight
     */
    public double getWeight(GraphElement o);

}
