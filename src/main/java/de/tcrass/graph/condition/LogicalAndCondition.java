package de.tcrass.graph.condition;

import de.tcrass.graph.GraphObject;

/**
 * A {@link GraphObjectCondition} combining the results of two other
 * test using logical AND.
 */
public class LogicalAndCondition extends LogicalCombinationCondition {

    /**
     * Creates a {@link LogicalCombinationCondition} combining the
     * results of the given tests using logical AND (negating: NAND).
     * 
     * @param cond1
     *            the one sub-condition
     * @param cond2
     *            the other sub-condition
     * @param negating
     *            whether or not to negate the test result
     */
    public LogicalAndCondition(GraphObjectCondition cond1, GraphObjectCondition cond2,
                               boolean negating) {
        super(cond1, cond2, negating);
    }

    /**
     * Creates a {@link LogicalCombinationCondition} combining the
     * results of the given tests using logical AND.
     * 
     * @param cond1
     *            the one sub-condition
     * @param cond2
     *            the other sub-condition
     */
    public LogicalAndCondition(GraphObjectCondition cond1, GraphObjectCondition cond2) {
        this(cond1, cond2, false);
    }

    /**
     * Tells whether both sub-conditions are simulatneously satisfied
     * (negation: at least one sub-condition is not satisfied).
     * 
     * @return the (possibly negated) result of ANDing sub-condition 1
     *         with sub-condition 2
     */
    public boolean satisfiedBy(GraphObject o) {
        return resultFor(cond1.satisfiedBy(o) && cond2.satisfiedBy(o));
    }

}
