package de.tcrass.graph.condition;

import de.tcrass.graph.GraphObject;

/**
 * A {@link GraphObjectCondition} combining the results of two other
 * test using logical OR.
 */
public class LogicalOrCondition extends LogicalCombinationCondition {

    /**
     * Creates a {@link LogicalCombinationCondition} combining the
     * results of the given tests using logical OR (negating: NOR).
     * 
     * @param cond1
     *            the one sub-condition
     * @param cond2
     *            the other sub-condition
     * @param negating
     *            whether or not to negate the test result
     */
    public LogicalOrCondition(GraphObjectCondition cond1, GraphObjectCondition cond2,
                              boolean negating) {
        super(cond1, cond2, negating);
    }

    /**
     * Creates a {@link LogicalCombinationCondition} combining the
     * results of the given tests using logical OR.
     * 
     * @param cond1
     *            the one sub-condition
     * @param cond2
     *            the other sub-condition
     */
    public LogicalOrCondition(GraphObjectCondition cond1, GraphObjectCondition cond2) {
        this(cond1, cond2, false);
    }

    /**
     * Tells whether at least one sub-condition ist satisfied (negation:
     * neither of the sub-conditions is satisfied).
     * 
     * @return the (possibly negated) result of ORing sub-condition 1
     *         with sub-condition 2
     */
    public boolean satisfiedBy(GraphObject o) {
        return resultFor(cond1.satisfiedBy(o) || cond2.satisfiedBy(o));
    }

}
