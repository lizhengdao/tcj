package de.tcrass.graph.condition;

import de.tcrass.graph.GraphObject;

/**
 * A {@link GraphObjectCondition} telling whether or not a {@link long}
 * item stored in its {@link de.tcrass.util.DataStore} and identified by
 * its key has (negation: doesn't have) a certain value
 */
public class LongDataCondition extends GraphObjectConditionImpl {

    private String key;
    private long   value;

    /**
     * Creates a GraphObjectCondition for checking whether a
     * GraphObject's data item identified by the given key has
     * (negation: doesn't have) the given {@link long} value
     * 
     * @param k
     *            the key identifying the data item to be examind within
     *            the GraphObject's DataStore
     * @param v
     *            the value the specified data item is to be checked for
     * @param negating
     *            whether or not to test for the opposite
     */
    public LongDataCondition(String k, long v, boolean negating) {
        super(negating);
        key = k;
        value = v;
    }

    /**
     * Creates a GraphObjectCondition for checking whether a
     * GraphObject's data item identified by the given key has the given
     * {@link long} value
     * 
     * @param k
     *            the key identifying the data item to be examind within
     *            the GraphObject's DataStore
     * @param v
     *            the value the specified data item is to be checked for
     */
    public LongDataCondition(String k, long v) {
        this(k, v, false);
    }

    /**
     * Tells whether or not the given GraphObject's data item identified
     * by the key given during instantiation has the value previously
     * specified
     * 
     * @param o
     *            the GraphObject under question
     * @return true, if the given GraphObject satisfies the above
     *         condition
     */
    public boolean satisfiedBy(GraphObject o) {
        return resultFor(o.data().getLong(key) == value);
    }

}
