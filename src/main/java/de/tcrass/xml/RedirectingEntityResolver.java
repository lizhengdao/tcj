/*
 * Created on Apr 23, 2005 TODO To change the template for this
 * generated file go to Window - Preferences - Java - Code Style - Code
 * Templates
 */
package de.tcrass.xml;

import java.util.Properties;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class RedirectingEntityResolver implements EntityResolver {

    Properties map;

    public RedirectingEntityResolver(Properties map) {
        this.map = map;
    }

    public RedirectingEntityResolver() {
        this(new Properties());
    }

    public void addRedirect(String source, String target) {
        map.setProperty(source, target);
    }

    public String getRedirect(String source) {
        return map.getProperty(source);
    }

    public Properties getRedirects() {
        return (Properties) map.clone();
    }

    public void removeRedirect(String source) {
        map.remove(source);
    }

    public InputSource resolveEntity(String publicId, String systemId) {
        InputSource is = null;
        if (map.getProperty(systemId) != null) {
            is = new InputSource(map.getProperty(systemId));
        }
        return is;
    }

}
