/*
 * Created on 26.03.2004 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing;

import de.tcrass.swing.panel.TListPanel;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public interface ListDoubleClickListener {

    void listDoubleClicked(TListPanel l);

}
