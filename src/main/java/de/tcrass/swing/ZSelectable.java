package de.tcrass.swing;

public interface ZSelectable {

    static final int IS_SELECTABLE      = 65536;
    static final int RAISE_ON_SELECTION = IS_SELECTABLE * 2;

    int getZSelectProperties();

    void onSelection();

    void onDeselection();

    void staySelected();

}
