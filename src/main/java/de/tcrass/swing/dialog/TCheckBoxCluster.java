/*
 * Created on 01.09.2003 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing.dialog;

import javax.swing.JCheckBox;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class TCheckBoxCluster extends TComponentCluster {

    JCheckBox box;

    public TCheckBoxCluster(String title) {
        super(title, 4);
        initCluster(false, 1);
    }

    public TCheckBoxCluster(String title, boolean value) {
        super(title, 4);
        initCluster(value, 1);
    }

    public TCheckBoxCluster(String title, int titleWidth, boolean value, int valueWidth) {
        super(title, titleWidth);
        initCluster(value, valueWidth);
    }

    private void initCluster(boolean value, int valueWidth) {
        box = new JCheckBox();
        addLeftMaxComponent(box, valueWidth);
        setChecked(value);
    }

    public JCheckBox getCheckBox() {
        return box;
    }

    public boolean isChecked() {
        return box.isSelected();
    }

    public void setChecked(boolean checked) {
        box.setSelected(checked);
    }

    @Override
    public String getValue() {
        return Boolean.valueOf(box.isSelected()).toString();
    }

}
