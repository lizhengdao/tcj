/*
 * Created on 29.08.2003 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing.dialog;

import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public abstract class TNumericCluster extends TStringCluster {

    private static final int DEFAULT_COLS = 12;

    public TNumericCluster(String title) {
        this(title, 4, "", 2);
    }

    public TNumericCluster(String title, String value) {
        this(title, 4, value, 2);
    }

    public TNumericCluster(String title, int titleWidth, int valueWidth) {
        this(title, titleWidth, "", valueWidth);
    }

    public TNumericCluster(String title, int titleWidth, String value, int valueWidth) {
        super(title, titleWidth, value, valueWidth, DEFAULT_COLS);
    }

    @Override
    protected void createInputComponent(String value, int valueWidth, int cols) {
        addLeftComponent(new JTextField(value, cols), valueWidth);
    }

    @Override
    protected void initCluster(String value, int valueWidth, int cols) {
        super.initCluster(value, valueWidth, cols);
        getTextField().setHorizontalAlignment(SwingConstants.RIGHT);
    }
}
