/*
 * Created on 29.08.2003 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing.dialog;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.border.Border;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class TStringCluster extends TComponentCluster {

    private static final int DEFAULT_COLS = 20;

    private Border           border;

    public TStringCluster(String title) {
        this(title, 2, "", 4, DEFAULT_COLS);
    }

    public TStringCluster(String title, String value) {
        this(title, 2, value, 4, DEFAULT_COLS);
    }

    public TStringCluster(String title, int titleWidth, String value, int valueWidth) {
        this(title, titleWidth, value, valueWidth, DEFAULT_COLS);
    }

    public TStringCluster(String title, int cols) {
        this(title, 2, "", 4, cols);
        createInputComponent("", 4, cols);
    }

    public TStringCluster(String title, String value, int cols) {
        this(title, 2, value, 4, cols);
    }

    public TStringCluster(String title, int titleWidth, String value, int valueWidth, int cols) {
        super(title, titleWidth);
        initCluster(value, valueWidth, cols);
    }

    protected void initCluster(String value, int valueWidth, int cols) {
        createInputComponent(value, valueWidth, cols);
        getTextField().addFocusListener(this);
        border = getTextField().getBorder();
    }

    protected void createInputComponent(String value, int valueWidth, int cols) {
        addRightMaxComponent(new JTextField(value, cols), valueWidth);
    }

    @Override
    protected void markAsValidInput(boolean valid) {
        if (valid) {
            getTextField().setBorder(border);
            getTextField().setToolTipText(null);
        }
        else {
            getTextField().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
            getTextField().setToolTipText(getInvalidInputTooltip());
        }
    }

    protected JTextField getTextField() {
        return (JTextField) getComponent(1);
    }

    @Override
    public String getValue() {
        return getTextField().getText();
    }

    public void setValue(String value) {
        getTextField().setText(value);
    }

}
