/*
 * Created on 10.09.2004 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.swing;

import java.awt.Dimension;

import javax.swing.JLabel;

import de.tcrass.util.StringUtils;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class FixedSizeLabel extends JLabel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private int               width;

    public FixedSizeLabel(int width) {
        this(width, "");
    }

    public FixedSizeLabel(int width, String text) {
        super();
        setWidth(width);
    }

    public void setWidth(int width) {
        this.width = width;
        invalidate();
    }

    @Override
    public Dimension getMinimumSize() {
        JLabel template = new JLabel(StringUtils.polymerize("#", width));
        return new Dimension(template.getPreferredSize().width, super.getPreferredSize().height);
    }

    @Override
    public Dimension getPreferredSize() {
        JLabel template = new JLabel(StringUtils.polymerize("#", width));
        return new Dimension(template.getPreferredSize().width, super.getPreferredSize().height);
    }

}
