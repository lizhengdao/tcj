/*
 * Created on 31.01.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.swing.panel;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import de.tcrass.swing.TToolBarButton;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class TBrowserControls extends JPanel {

    /**
     * 
     */
    private static final long   serialVersionUID       = 1L;
    public static final int     HOME_BUTTON            = 1;
    public static final int     BACK_BUTTON            = 2;
    public static final int     FORWARD_BUTTON         = 4;
    public static final int     GOTO_BUTTON            = 8;
    public static final int     RELOAD_BUTTON          = 16;
    public static final int     CLOSE_BUTTON           = 128;

    public static final int     NAVIGATION_BUTTONS     = HOME_BUTTON + BACK_BUTTON +
                                                         FORWARD_BUTTON;

    public static final int     ALL_BUT_CLOSE_BUTTON   = HOME_BUTTON + BACK_BUTTON +
                                                         FORWARD_BUTTON + GOTO_BUTTON +
                                                         RELOAD_BUTTON;

    public static final int     ALL_BUTTONS            = ALL_BUT_CLOSE_BUTTON + CLOSE_BUTTON;

    private static final String DEFAULT_RESOURCES_PATH = "org.comlink.home.tcrass.swing.resources.browser";

    private THTMLBrowserPanel   browser;

    private TToolBarButton      homeButton;
    private TToolBarButton      backButton;
    private TToolBarButton      forwardButton;
    private TToolBarButton      goToButton;
    private TToolBarButton      reloadButton;
    private TToolBarButton      closeButton;

    TBrowserControls(THTMLBrowserPanel b, int buttons) {
        super();
        browser = b;

        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

        homeButton = TToolBarButton.createIconButton("browserHome", "Return to home page",
            DEFAULT_RESOURCES_PATH);
        homeButton.addActionListener(browser);
        if ((buttons & HOME_BUTTON) != 0) {
            add(homeButton);
        }

        backButton = TToolBarButton.createIconButton("browserBack", "Return to previous page",
            DEFAULT_RESOURCES_PATH);
        backButton.addActionListener(browser);
        if ((buttons & BACK_BUTTON) != 0) {
            add(backButton);
        }

        forwardButton = TToolBarButton.createIconButton("browserForward",
            "Move forward to next page", DEFAULT_RESOURCES_PATH);
        forwardButton.addActionListener(browser);
        if ((buttons & FORWARD_BUTTON) != 0) {
            add(forwardButton);
        }

        goToButton = TToolBarButton.createIconButton("browserGoTo", "Open location...",
            DEFAULT_RESOURCES_PATH);
        goToButton.addActionListener(browser);
        if ((buttons & GOTO_BUTTON) != 0) {
            add(goToButton);
        }

        reloadButton = TToolBarButton.createIconButton("browserReload",
            "Reload current document", DEFAULT_RESOURCES_PATH);
        reloadButton.addActionListener(browser);
        if ((buttons & RELOAD_BUTTON) != 0) {
            add(reloadButton);
        }

        closeButton = TToolBarButton.createIconButton("browserClose", "Close browser window",
            DEFAULT_RESOURCES_PATH);
        closeButton.addActionListener(browser);
        if ((buttons & CLOSE_BUTTON) != 0) {
            add(Box.createHorizontalGlue());
            add(closeButton);
        }
    }

    public void disableButtons() {
        homeButton.setEnabled(false);
        backButton.setEnabled(false);
        forwardButton.setEnabled(false);
        goToButton.setEnabled(false);
    }

    public void updateButtons() {
        homeButton.setEnabled(browser.getHomeURL() != null);
        backButton.setEnabled(browser.getHistory().hasPrevious());
        forwardButton.setEnabled(browser.getHistory().hasNext());
        goToButton.setEnabled(true);
    }

}
