/*
 * Created on 17.09.2003 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.lang.reflect.InvocationTargetException;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import de.tcrass.swing.dialog.TDialog;
import de.tcrass.util.StringUtils;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class SwingUtils {

    public static void invokeAndWait(Runnable r) throws InterruptedException,
    InvocationTargetException {
        if (SwingUtilities.isEventDispatchThread()) {
            r.run();
        }
        else {
            SwingUtilities.invokeAndWait(r);
        }
    }

    public static void invokeLater(Runnable r) {
        if (SwingUtilities.isEventDispatchThread()) {
            // r.run();
            SwingUtilities.invokeLater(r);
            // ThreadPool.getDefaultThreadPool().addTask(r);
        }
        else {
            SwingUtilities.invokeLater(r);
        }
    }

    public static JPanel createSimpleDialogPanel(String text, int minLen) {
        JPanel p = new JPanel();
        p.setBorder(BorderFactory.createEmptyBorder(16, 16, 16, 16));
        JLabel l = new JLabel(StringUtils.toHTMLCaption(StringUtils.insertSystemLinebreaks(
            text, minLen)));
        p.add(l);
        return p;
    }

    public static boolean warningDialog(String caption, String text, int minLen, Frame f) {
        TDialog d = new TDialog(caption, TDialog.YES_BUTTON | TDialog.NO_BUTTON,
            TDialog.NO_BUTTON, TDialog.WARNING_ICON, createSimpleDialogPanel(text, minLen), f);
        ;
        return (d.showDialog() == TDialog.YES_BUTTON);
    }

    public static boolean warningDialog(String caption, String text, int minLen, Dialog f) {
        TDialog d = new TDialog(caption, TDialog.YES_BUTTON | TDialog.NO_BUTTON,
            TDialog.NO_BUTTON, TDialog.WARNING_ICON, createSimpleDialogPanel(text, minLen), f);
        ;
        return (d.showDialog() == TDialog.YES_BUTTON);
    }

    public static void infoDialog(String caption, String text, int minLen, Frame f) {
        TDialog d = new TDialog(caption, TDialog.OK_BUTTON, TDialog.OK_BUTTON,
            TDialog.INFORMATION_ICON, createSimpleDialogPanel(text, minLen), f);
        ;
        d.showDialog();
    }

    public static void infoDialog(String caption, String text, int minLen, Dialog f) {
        TDialog d = new TDialog(caption, TDialog.OK_BUTTON, TDialog.OK_BUTTON,
            TDialog.INFORMATION_ICON, createSimpleDialogPanel(text, minLen), f);
        ;
        d.showDialog();
    }

    public static boolean yesNoDialog(String caption, String text, int minLen, Frame f) {
        TDialog d = new TDialog(caption, TDialog.YES_BUTTON + TDialog.NO_BUTTON,
            TDialog.NO_BUTTON, TDialog.QUESTION_ICON, createSimpleDialogPanel(text, minLen), f);
        ;
        return (d.showDialog() == TDialog.YES_BUTTON);
    }

    public static boolean yesNoDialog(String caption, String text, int minLen, Dialog f) {
        TDialog d = new TDialog(caption, TDialog.YES_BUTTON + TDialog.NO_BUTTON,
            TDialog.NO_BUTTON, TDialog.QUESTION_ICON, createSimpleDialogPanel(text, minLen), f);
        ;
        return (d.showDialog() == TDialog.YES_BUTTON);
    }

    public static boolean okCancelDialog(String caption, String text, int minLen, Frame f) {
        TDialog d = new TDialog(caption, TDialog.OK_BUTTON + TDialog.CANCEL_BUTTON,
            TDialog.CANCEL_BUTTON, TDialog.WARNING_ICON, createSimpleDialogPanel(text, minLen),
            f);
        ;
        return (d.showDialog() == TDialog.OK_BUTTON);
    }

    public static boolean okCancelDialog(String caption, String text, int minLen, Dialog f) {
        TDialog d = new TDialog(caption, TDialog.OK_BUTTON + TDialog.CANCEL_BUTTON,
            TDialog.CANCEL_BUTTON, TDialog.WARNING_ICON, createSimpleDialogPanel(text, minLen),
            f);
        ;
        return (d.showDialog() == TDialog.OK_BUTTON);
    }

    public static int yesNoCancelDialog(String caption, String text, int minLen, Frame f) {
        TDialog d = new TDialog(caption, TDialog.YES_BUTTON + TDialog.NO_BUTTON +
                                         TDialog.CANCEL_BUTTON, TDialog.CANCEL_BUTTON,
            TDialog.WARNING_ICON, createSimpleDialogPanel(text, minLen), f);
        ;
        return d.showDialog();
    }

    public static int yesNoCancelDialog(String caption, String text, int minLen, Dialog f) {
        TDialog d = new TDialog(caption, TDialog.YES_BUTTON + TDialog.NO_BUTTON +
                                         TDialog.CANCEL_BUTTON, TDialog.CANCEL_BUTTON,
            TDialog.WARNING_ICON, createSimpleDialogPanel(text, minLen), f);
        ;
        return d.showDialog();
    }

    public static Component getInitialInvoker(JPopupMenu pm) {
        Component ic = pm.getInvoker();
        if (ic == null) {
            ic = pm;
        }
        else {
            if (ic instanceof JMenuItem) {
                Container p = ic.getParent();
                if (p instanceof JPopupMenu) {
                    ic = getInitialInvoker((JPopupMenu) p);
                }
            }
        }
        return ic;
    }

    public static Component getInitialMenu(JPopupMenu pm) {
        Component ic = pm.getInvoker();
        if (ic instanceof JMenuItem) {
            Container p = ic.getParent();
            if (p instanceof JPopupMenu) {
                ic = getInitialMenu((JPopupMenu) p);
            }
        }
        else {
            ic = pm;
        }
        return ic;
    }

}
