/*
 * Created on 26.01.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.t3d;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.image.BufferedImage;

import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.awt.GLJPanel;
import com.jogamp.opengl.util.awt.AWTGLReadBufferUtil;

import de.tcrass.util.SysUtils;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class JOGLCubeMapper implements CubeMapper {

    public static final GLProfile GL_PROFILE = GLProfile.getGL2ES1();
    
    private static final AWTGLReadBufferUtil SCREENSHOT = new AWTGLReadBufferUtil(GL_PROFILE, false);

    private double                 theta, phi;
    private BufferedImage[]        map         = null;
    private Dimension              canvasSize  = null;
    private boolean                lightweight = true;
    private GLAutoDrawable         drawable;
    private GLCanvas               canvas;
    private GLJPanel               jpanel;
    private JOGLCubeMapperListener listener;
    private GLCapabilities         caps;

    // private JOGLCubeMapper shadow = null;
    // private JFrame sf = null;

    // --- Constructors ---

    public JOGLCubeMapper() {
        this(false);
    }

    public JOGLCubeMapper(boolean lw) {
        lightweight = lw;
    }

    // --- Private methods

    // --- Package-scope methods

    Dimension getSize() {
        return canvasSize;
    }

    double getTheta() {
        return theta;
    }

    double getPhi() {
        return phi;
    }

    BufferedImage[] getMap() {
        return map;
    }

    // --- Implementation of CubeMapper

    public void setMap(BufferedImage[] images) {
        map = images;
        if (listener != null) {
            listener.dispose();
        }
    }

    public Dimension getCanvasSize() {
        return canvasSize;
    }

    public void setCanvasSize(Dimension d) {
        canvasSize = d;
        if (drawable != null) {
            getComponent().setSize(d.width, d.height);
        }
    }

    public void setRotation(double t, double p) {
        theta = t;
        phi = p;
        if (drawable != null) {
            drawable.display();
        }
    }

    public Component getCanvas() {
        if (drawable == null) {
            caps = new GLCapabilities(GL_PROFILE);
            if (lightweight) {
                jpanel = new GLJPanel(caps);
                drawable = jpanel;
            }
            else {
                canvas = new GLCanvas(caps);
                drawable = canvas;
            }
            listener = new JOGLCubeMapperListener(this, drawable);
            drawable.addGLEventListener(listener);
            if (canvasSize != null) {
                getComponent().setSize(canvasSize.width, canvasSize.height);
            }
        }

        return (Component) drawable;
    }

    private Component getComponent() {
        return lightweight ? jpanel : canvas;
    }
    
    public BufferedImage getScreenshot() {
        BufferedImage img = null;
        if (drawable != null) {
            try {
                drawable.getContext().makeCurrent();
                drawable.display();
                img = SCREENSHOT.readPixelsToBufferedImage(drawable.getGL(), true);
            }
            catch (Exception ex) {
                SysUtils.doNothing();
            }
        }
        return img;
    }

    public void dispose() {
        if (drawable != null) {
            drawable.removeGLEventListener(listener);
            listener.dispose();
        }
    }

}
