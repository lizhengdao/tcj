package de.tcrass.t3d;

import java.awt.Dimension;
import java.awt.Point;

public class T3DUtils {

    public static final int X           = 0;
    public static final int Y           = 1;
    public static final int Z           = 2;

    public static final int NO_OF_FACES = 6;

    public static final int LEFT        = 0;
    public static final int FRONT       = 1;
    public static final int RIGHT       = 2;
    public static final int BACK        = 3;
    public static final int UP          = 4;
    public static final int DOWN        = 5;

    public static float[] sphericalSurface(float theta, float phi) {
        float[] r = new float[3];
        double t = theta * Math.PI / 180;
        double p = -phi * Math.PI / 180;
        r[X] = (float) (Math.sin(t) * Math.cos(p));
        r[Y] = (float) (Math.sin(p));
        r[Z] = (float) (Math.cos(t) * Math.cos(p));
        return r;
    }

    public static float[] rotateX(float[] r, float alpha) {
        double a = alpha * Math.PI / 180;
        float[] p = new float[3];
        p[X] = r[X];
        p[Y] = (float) (r[Y] * Math.cos(a) - r[Z] * Math.sin(a));
        p[Z] = (float) (r[Z] * Math.cos(a) + r[Y] * Math.sin(a));
        return p;
    }

    public static float[] rotateY(float[] r, float alpha) {
        double a = alpha * Math.PI / 180;
        float[] p = new float[3];
        p[X] = (float) (r[X] * Math.cos(a) + r[Z] * Math.sin(a));
        p[Y] = r[Y];
        p[Z] = (float) (r[Z] * Math.cos(a) - r[X] * Math.sin(a));
        return p;
    }

    public static Point project(float[] r, float d, Dimension s) {
        Point p = null;
        if (r[Z] > 0) {
            p = new Point();
            // p.x = (int)(s.width * (d * r[X] + 0.5));
            // p.y = (int)(s.height * (d * r[Y] + 0.5));
            p.x = (int) (s.width * (d / 2 * r[X] / r[Z] + 0.5));
            p.y = (int) (s.height * (d / 2 * r[Y] / r[Z] + 0.5));
        }
        return p;
    }

    public static Point spherical2cartesian(float theta, float phi, Dimension d, float alpha,
                                            float beta) {
        float[] r = new float[3];
        r = T3DUtils.sphericalSurface(theta, phi);
        r = T3DUtils.rotateY(r, -alpha);
        r = T3DUtils.rotateX(r, -beta);
        return T3DUtils.project(r, 1, d);
    }

    public static float cartesian2theta(Point p, Dimension d, float alpha, float beta) {
        double x = (float) p.x / (float) (d.width) - 0.5;
        float theta = (float) (Math.atan(2 * x) * 180 / Math.PI);
        double y = (float) p.y / (float) (d.height) - 0.5;
        float phi = -(float) (Math.atan(2 * y) * 180 / Math.PI);
        float[] r = new float[3];
        r = T3DUtils.sphericalSurface(theta, phi);
        r = T3DUtils.rotateX(r, beta);
        r = T3DUtils.rotateY(r, alpha);
        theta = (float) (Math.atan2(r[X], r[Z]) * 180 / Math.PI + 360) % 360;
        return theta;
    }

    public static float cartesian2phi(Point p, Dimension d, float alpha, float beta) {
        double x = (float) p.x / (float) (d.width) - 0.5;
        float theta = (float) (Math.atan(2 * x) * 180 / Math.PI);
        double y = (float) p.y / (float) (d.height) - 0.5;
        float phi = -(float) (Math.atan(2 * y) * 180 / Math.PI);
        float[] r = new float[3];
        r = T3DUtils.sphericalSurface(theta, phi);
        r = T3DUtils.rotateX(r, beta);
        r = T3DUtils.rotateY(r, alpha);
        phi = -(float) (Math.asin(r[Y]) * 180 / Math.PI);
        return phi;
    }

}
