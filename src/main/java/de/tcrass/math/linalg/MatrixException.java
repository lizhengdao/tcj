package de.tcrass.math.linalg;

import de.tcrass.math.MathException;

public class MatrixException extends MathException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Matrix            A;

    public MatrixException(Matrix A, String msg) {
        super(msg);
        this.A = A;
    }

    public Matrix getMatrix() {
        return A;
    }

}
