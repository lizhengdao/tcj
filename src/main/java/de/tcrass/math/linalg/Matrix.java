package de.tcrass.math.linalg;

import java.awt.Point;
import java.text.DecimalFormat;
import java.util.Random;

import de.tcrass.util.StringUtils;

public class Matrix implements Cloneable {

    protected int               mRows;                // rows
    protected int               nCols;                // cols
    private int                 size;
    private double[]            elems;

    private static final Random random = new Random();

    public Matrix(int rows, int cols) {
        mRows = rows;
        nCols = cols;
        size = rows * cols;
        elems = new double[size];
    }

    // --- Static methods

    public static Matrix Null(int mRows, int nCols) {
        return new Matrix(mRows, nCols);
    }

    public static Matrix Null(int dim) {
        return new Matrix(dim, dim);
    }

    public static Matrix Unit(int dim) {
        Matrix One = Matrix.Null(dim);
        for (int i = 1; i <= dim; i++) {
            One.set(i, i, 1);
        }
        return One;
    }

    public static Matrix Random(int mRows, int nCols, double lower, double upper, Random r) {
        Matrix Random = new Matrix(mRows, nCols);
        double delta = upper - lower;
        double[] elems = Random.getArray();
        for (int i = 0; i < elems.length; i++) {
            elems[i] = lower + delta * r.nextDouble();
        }
        return Random;
    }

    public static Matrix Random(int mRows, int nCols, double lower, double upper) {
        return Matrix.Random(mRows, nCols, lower, upper, random);
    }

    public static Matrix Random(int mRows, int nCols, int lower, int upper, Random r) {
        Matrix Random = new Matrix(mRows, nCols);
        int delta = upper - lower;
        double[] elems = Random.getArray();
        for (int i = 0; i < elems.length; i++) {
            elems[i] = lower + r.nextInt(delta);
        }
        return Random;
    }

    public static Matrix Random(int mRows, int nCols, int lower, int upper) {
        return Matrix.Random(mRows, nCols, lower, upper, random);
    }

    // --- Private methods

    // --- Protected methods

    public MatrixException newException(String msg) {
        return new MatrixException(this, msg);
    }

    protected double[] getArray() {
        return elems;
    }

    protected void setArray(double[] newElems, int rows, int cols) {
        int n = rows * cols;
        if (newElems.length != n) {
            throw newException("Size of new array incompatible with matrix dimensions.");
        }
        else {
            size = n;
            mRows = rows;
            nCols = cols;
            elems = newElems;
        }
    }

    protected void doTranspose(double[] newElems) {
        int addr = 0;
        for (int j = 0; j < mRows; j++) {
            for (int i = 0; i < nCols; i++) {
                newElems[j + mRows * i] = elems[addr];
                addr++;
            }
        }
    }

    protected void doMultiply(double[] elems, double a) {
        for (int i = 0; i < elems.length; i++) {
            elems[i] = a * elems[i];
        }
    }

    protected void doDivide(double[] elems, double a) {
        for (int i = 0; i < elems.length; i++) {
            elems[i] = elems[i] / a;
        }
    }

    protected void doAdd(double[] elems, double a) {
        for (int i = 0; i < elems.length; i++) {
            elems[i] = elems[i] + a;
        }
    }

    protected void doSubstract(double[] elems, double a) {
        for (int i = 0; i < elems.length; i++) {
            elems[i] = elems[i] - a;
        }
    }

    protected void doNegate(double[] newElems) {
        for (int i = 0; i < newElems.length; i++) {
            newElems[i] = -newElems[i];
        }
    }

    protected int doFindMax(double[] elems) {
        double max = Double.NEGATIVE_INFINITY;
        int addr = 0;
        for (int i = 0; i < elems.length; i++) {
            if (elems[i] > max) {
                max = elems[i];
                addr = i;
            }
        }
        return addr;
    }

    protected int doFindMin(double[] elems) {
        double min = Double.POSITIVE_INFINITY;
        int addr = 0;
        for (int i = 0; i < elems.length; i++) {
            if (elems[i] < min) {
                min = elems[i];
                addr = i;
            }
        }
        return addr;
    }

    protected int doFindMaxAbs(double[] elems) {
        double max = 0;
        int addr = 0;
        for (int i = 0; i < elems.length; i++) {
            if (Math.abs(elems[i]) > max) {
                max = Math.abs(elems[i]);
                addr = i;
            }
        }
        return addr;
    }

    protected int doFindMinAbs(double[] elems) {
        double min = Double.POSITIVE_INFINITY;
        int addr = 0;
        for (int i = 0; i < elems.length; i++) {
            if (Math.abs(elems[i]) < min) {
                min = Math.abs(elems[i]);
                addr = i;
            }
        }
        return addr;
    }

    // --- Package-scope methods

    // --- Public methods

    public int mRows() {
        return mRows;
    }

    public int nCols() {
        return nCols;
    }

    public int addr(int row, int col) {
        if ((row < 1) || (row > mRows) || (col < 1) || (col > nCols)) {
            throw newException("Attempt to access matrix element at off-index position");
        }
        return nCols * (row - 1) + (col - 1);
    }

    public Point pos(int addr) {
        if ((addr < 0) || (addr >= size)) {
            throw newException("Attempt to access matrix element at off-index position");
        }
        int i = addr / nCols;
        int j = addr % nCols;
        return new Point(j + 1, i + 1);
    }

    @Override
    public String toString() {
        return toString(4, 3);
    }

    public String toString(int p, int q) {
        int l = p + q + 2;
        String fs = StringUtils.polymerize("#", p - 1) + "0";
        if (q > 0) {
            fs = fs + "." + StringUtils.polymerize("0", q);
        }
        DecimalFormat df = new DecimalFormat(fs);
        StringBuffer s = new StringBuffer();
        for (int i = 1; i <= mRows; i++) {
            if (mRows == 1) {
                s.append("( ");
            }
            else {
                if (i == 1) {
                    s.append("/ ");
                }
                else if (i == mRows) {
                    s.append("\\ ");
                }
                else {
                    s.append("| ");
                }
            }
            for (int j = 1; j <= nCols; j++) {
                String ds = df.format(get(i, j));
                if (ds.length() > l) {
                    ds = StringUtils.polymerize("#", l);
                }
                s.append(StringUtils.polymerize(" ", l - ds.length()) + ds + " ");
            }
            if (mRows == 1) {
                s.append(")");
            }
            else {
                if (i == 1) {
                    s.append("\\");
                }
                else if (i == mRows) {
                    s.append("/");
                }
                else {
                    s.append("|");
                }
            }
            s.append("\n");
        }
        return s.toString();
    }

    public double[] asArray() {
        return getArray().clone();
    }

    public double get(int row, int col) {
        return elems[addr(row, col)];
    }

    public double get(int addr) {
        if ((addr < 0) || (addr >= size)) {
            throw newException("Attempt to access matrix element at off-index position");
        }
        return elems[addr];
    }

    public void set(int row, int col, double d) {
        elems[addr(row, col)] = d;
    }

    public void set(int addr, double a) {
        if ((addr < 0) || (addr >= size)) {
            throw newException("Attempt to access matrix element at off-index position");
        }
        else {
            elems[addr] = a;
        }
    }

    // --- Row manipulation

    public double[] getRow(int i) {
        int addr = addr(i, 1);
        double[] row = new double[nCols];
        for (int j = 0; j < nCols; j++) {
            row[j] = elems[addr];
            addr++;
        }
        return row;
    }

    public RowVector getRowVector(int i) {
        return new RowVector(getRow(i));
    }

    public double[][] getRows() {
        double[][] rows = new double[mRows][nCols];
        int addr = 0;
        for (int i = 0; i < mRows; i++) {
            for (int j = 0; j < nCols; j++) {
                rows[i][j] = elems[addr];
                addr++;
            }
        }
        return rows;
    }

    public RowVector[] getRowVectors() {
        RowVector[] rows = new RowVector[mRows];
        for (int i = 1; i <= mRows; i++) {
            rows[i - 1] = getRowVector(i);
        }
        return rows;
    }

    public void setRow(int at, double[] newRow) {
        if ((at < 1) || (at > (mRows + 1))) {
            throw newException("Attempt to mopdify row at off-index position.");
        }
        else if (newRow.length != nCols) {
            throw newException("Attempt to modify row with incompatible number of elements.");
        }
        int addr = (at - 1) * nCols;
        for (int j = 0; j < nCols; j++) {
            elems[addr] = newRow[j];
            addr++;
        }
    }

    public void setRow(int at, RowVector newRow) {
        setRow(at, newRow.getArray());
    }

    public void setRows(double[][] rows) {
        if (rows.length != mRows) {
            throw newException("Attempt to set incompatible number of rows.");
        }
        int addr = 0;
        for (int i = 0; i < mRows; i++) {
            if (rows[i].length != nCols) {
                throw newException("Attempt to modify row with incompatible number of elements.");
            }
            else {
                for (int j = 0; j < nCols; j++) {
                    elems[addr] = rows[i][j];
                    addr++;
                }
            }
        }
    }

    public void setRows(RowVector[] rows) {
        if (rows.length != mRows) {
            throw newException("Attempt to set incompatible number of rows.");
        }
        else {
            for (int i = 0; i < mRows; i++) {
                setRow(i + 1, rows[i].getArray());
            }
        }
    }

    public void addRow(double[] newRow) {
        insertRow(mRows + 1, newRow);
    }

    public void addRow(RowVector newRow) {
        addRow(newRow.getArray());
    }

    public void insertRow(int at, double[] newRow) {
        if ((at < 1) || (at > (mRows + 1))) {
            throw newException("Attempt to insert row at off-index position.");
        }
        else if (newRow.length != nCols) {
            throw newException("Attempt to insert row with incompatible number of elements.");
        }
        else {
            at--;
            double[] newElems = new double[size + nCols];
            int addr1 = 0;
            int addr2 = 0;
            for (int i = 0; i <= mRows; i++) {
                if (i == at) {
                    for (int j = 0; j < nCols; j++) {
                        newElems[addr2] = newRow[j];
                        addr2++;
                    }
                }
                if (addr1 < size) {
                    for (int j = 0; j < nCols; j++) {
                        newElems[addr2] = elems[addr1];
                        addr1++;
                        addr2++;
                    }
                }
            }
            setArray(newElems, mRows + 1, nCols);
        }
    }

    public void insertRow(int at, RowVector newRow) {
        insertRow(at, newRow.getArray());
    }

    public double[] removeRow(int at) {
        double[] row = null;
        if ((at < 1) || (at > mRows)) {
            throw newException("Attempt to remove row from off-index position.");
        }
        else {
            at--;
            row = new double[nCols];
            double[] newElems = new double[size - nCols];
            int addr1 = 0;
            int addr2 = 0;
            for (int i = 0; i < mRows; i++) {
                if (i == at) {
                    for (int j = 0; j < nCols; j++) {
                        row[j] = elems[addr1];
                        addr1++;
                    }
                }
                else {
                    for (int j = 0; j < nCols; j++) {
                        newElems[addr2] = elems[addr1];
                        addr1++;
                        addr2++;
                    }
                }
            }
            setArray(newElems, mRows - 1, nCols);
        }
        return row;
    }

    public RowVector removeRowVector(int at) {
        return new RowVector(removeRow(at));
    }

    public void swapRows(int row1, int row2) {
        if ((row1 < 1) || (row1 > mRows)) {
            throw newException("Attempt to swap row(s) at off-index position.");
        }
        else if ((row2 < 1) || (row2 > mRows)) {
            throw newException("Attempt to swap row(s) at off-index position.");
        }
        else {
            int addr1 = (row1 - 1) * nCols;
            int addr2 = (row2 - 1) * nCols;
            double tmp;
            for (int j = 0; j < nCols; j++) {
                tmp = elems[addr1];
                elems[addr1] = elems[addr2];
                elems[addr2] = tmp;
                addr1++;
                addr2++;
            }
        }
    }

    // --- Column manipulation

    public double[] getCol(int j) {
        int addr = addr(1, j);
        double[] col = new double[mRows];
        for (int i = 0; i < mRows; i++) {
            col[i] = elems[addr];
            addr += nCols;
        }
        return col;
    }

    public ColVector getColVector(int i) {
        return new ColVector(getCol(i));
    }

    public double[][] getCols() {
        double[][] cols = new double[nCols][mRows];
        int addr = 0;
        for (int j = 0; j < nCols; j++) {
            addr = j;
            for (int i = 0; i < mRows; i++) {
                cols[j][i] = elems[addr];
                addr += nCols;
            }
        }
        return cols;
    }

    public ColVector[] getColVectors() {
        ColVector[] cols = new ColVector[mRows];
        for (int j = 1; j <= nCols; j++) {
            cols[j - 1] = getColVector(j);
        }
        return cols;
    }

    public void setCol(int at, double[] newCol) {
        if ((at < 1) || (at > (nCols + 1))) {
            throw newException("Attempt to mopdify column at off-index position.");
        }
        else if (newCol.length != mRows) {
            throw newException("Attempt to modify column with incompatible number of elements.");
        }
        int addr = 0;
        for (int i = 0; i < mRows; i++) {
            elems[addr] = newCol[i];
            addr += nCols;
        }
    }

    public void setCol(int at, ColVector newCol) {
        setCol(at, newCol.getArray());
    }

    public void setCols(double[][] cols) {
        if (cols.length != nCols) {
            throw newException("Attempt to set incompatible number of columns.");
        }
        else {
            int addr = 0;
            for (int j = 0; j < nCols; j++) {
                addr = j;
                if (cols[j].length != mRows) {
                    throw newException("Attempt to modify column with incompatible number of elements.");
                }
                else {
                    for (int i = 0; i < mRows; i++) {
                        elems[addr] = cols[j][i];
                        addr += nCols;
                    }
                }
            }
        }
    }

    public void setCols(ColVector[] cols) {
        if (cols.length != nCols) {

        }
        else {
            for (int j = 0; j < nCols; j++) {
                setCol(j + 1, cols[j].getArray());
            }
        }
    }

    public void addCol(double[] newCol) {
        insertCol(nCols + 1, newCol);
    }

    public void addCol(ColVector newCol) {
        addCol(newCol.getArray());
    }

    public void insertCol(int at, double[] newCol) {
        if ((at < 1) || (at > (nCols + 1))) {
            throw newException("Attempt to insert column at off-index position.");
        }
        else if (newCol.length != mRows) {
            throw newException("Attempt to insert column with incompatible number of elements.");
        }
        else {
            double[] newElems = new double[size + mRows];
            at--;
            int newCols = nCols + 1;
            int start = 0;
            for (int j = 0; j <= nCols; j++) {
                int addr1 = j;
                int addr2 = start;
                if (j == at) {
                    for (int i = 0; i < mRows; i++) {
                        newElems[addr2] = newCol[i];
                        addr2 += newCols;
                    }
                    start++;
                    addr2 = start;
                }
                if (j < nCols) {
                    for (int i = 0; i < mRows; i++) {
                        newElems[addr2] = elems[addr1];
                        addr1 += nCols;
                        addr2 += newCols;
                    }
                }
                start++;
            }
            setArray(newElems, mRows, newCols);
        }
    }

    public void insertCol(int at, ColVector newCol) {
        insertCol(at, newCol.getArray());
    }

    public double[] removeCol(int at) {
        double[] col = null;
        if ((at < 1) || (at > nCols)) {
            throw newException("Attempt to remove column from off-index position.");
        }
        else {
            at--;
            col = new double[mRows];
            double[] newElems = new double[size - mRows];
            int newCols = nCols - 1;
            int start = 0;
            for (int j = 0; j < nCols; j++) {
                int addr1 = j;
                if (j == at) {
                    for (int i = 0; i < mRows; i++) {
                        col[i] = elems[addr1];
                        addr1 += nCols;
                    }
                }
                else {
                    int addr2 = start;
                    for (int i = 0; i < mRows; i++) {
                        newElems[addr2] = elems[addr1];
                        addr1 += nCols;
                        addr2 += newCols;
                    }
                    start++;
                }
            }
            setArray(newElems, mRows, newCols);
        }
        return col;
    }

    public ColVector removeColVector(int at) {
        return new ColVector(removeCol(at));
    }

    public void swapCols(int col1, int col2) {
        if ((col1 < 1) || (col1 > nCols)) {
            throw newException("Attempt to swap col(s) at off-index position.");
        }
        else if ((col2 < 1) || (col2 > nCols)) {
            throw newException("Attempt to swap cols(s) at off-index position.");
        }
        else {
            int addr1 = col1 - 1;
            int addr2 = col2 - 1;
            double tmp;
            for (int i = 0; i < mRows; i++) {
                tmp = elems[addr1];
                elems[addr1] = elems[addr2];
                elems[addr2] = tmp;
                addr1 += nCols;
                addr2 += nCols;
            }
        }
    }

    // --- Algebra operations altering this matrix

    public Matrix transpose() {
        double[] newElems = new double[size];
        doTranspose(newElems);
        setArray(newElems, nCols, mRows);
        return this;
    }

    public Matrix invert() {
        if (nCols != mRows) {
            throw newException("Cannot invert a non-square matrix.");
        }
        else {
            SLE.solve(this, Matrix.Unit(nCols));
            return this;
        }
    }

    public Matrix multiplyBy(double a) {
        doMultiply(getArray(), a);
        return this;
    }

    public Matrix divideBy(double a) {
        doDivide(getArray(), a);
        return this;
    }

    public Matrix add(double a) {
        doAdd(getArray(), a);
        return this;
    }

    public Matrix substract(double a) {
        doSubstract(getArray(), a);
        return this;
    }

    public Matrix negate() {
        doNegate(getArray());
        return this;
    }

    public Matrix multiplyRowBy(int at, double a) {
        if ((at < 1) || (at > mRows)) {
            throw newException("Attempt to manipulate row at off-index position.");
        }
        else {
            int addr = (at - 1) * nCols;
            for (int j = 0; j < nCols; j++) {
                elems[addr] = a * elems[addr];
                addr++;
            }
        }
        return this;
    }

    public Matrix divideRowBy(int at, double a) {
        if ((at < 1) || (at > mRows)) {
            throw newException("Attempt to manipulate row at off-index position.");
        }
        else {
            int addr = (at - 1) * nCols;
            for (int j = 0; j < nCols; j++) {
                elems[addr] = elems[addr] / a;
                addr++;
            }
        }
        return this;
    }

    public Matrix addToRow(int at, double a) {
        if ((at < 1) || (at > mRows)) {
            throw newException("Attempt to manipulate row at off-index position.");
        }
        else {
            int addr = (at - 1) * nCols;
            for (int j = 0; j < nCols; j++) {
                elems[addr] = elems[addr] + a;
                addr++;
            }
        }
        return this;
    }

    public Matrix subtractFromRow(int at, double a) {
        if ((at < 1) || (at > mRows)) {
            throw newException("Attempt to manipulate row at off-index position.");
        }
        else {
            int addr = (at - 1) * nCols;
            for (int j = 0; j < nCols; j++) {
                elems[addr] = elems[addr] - a;
                addr++;
            }
        }
        return this;
    }

    public Matrix negateRow(int at) {
        if ((at < 1) || (at > mRows)) {
            throw newException("Attempt to manipulate row at off-index position.");
        }
        else {
            int addr = (at - 1) * nCols;
            for (int j = 0; j < nCols; j++) {
                elems[addr] = -elems[addr];
                addr++;
            }
        }
        return this;
    }

    public Matrix multiplyRowBy(int at, double[] a) {
        if ((at < 1) || (at > mRows)) {
            throw newException("Attempt to manipulate row at off-index position.");
        }
        else if (a.length != nCols) {
            throw newException("Attempt to add row with incompatible size.");
        }
        else {
            int addr = (at - 1) * nCols;
            for (int j = 0; j < nCols; j++) {
                elems[addr] = a[j] * elems[addr];
                addr++;
            }
        }
        return this;
    }

    public Matrix multiplyRowBy(int at, RowVector a) {
        return multiplyRowBy(at, a.getArray());
    }

    public Matrix divideRowBy(int at, double[] a) {
        if ((at < 1) || (at > mRows)) {
            throw newException("Attempt to manipulate row at off-index position.");
        }
        else if (a.length != nCols) {
            throw newException("Attempt to add row with incompatible size.");
        }
        else {
            int addr = (at - 1) * nCols;
            for (int j = 0; j < nCols; j++) {
                elems[addr] = elems[addr] / a[j];
                addr++;
            }
        }
        return this;
    }

    public Matrix divideRowBy(int at, RowVector a) {
        return divideRowBy(at, a.getArray());
    }

    public Matrix addToRow(int at, double[] a) {
        if ((at < 1) || (at > mRows)) {
            throw newException("Attempt to manipulate row at off-index position.");
        }
        else if (a.length != nCols) {
            throw newException("Attempt to add row with incompatible size.");
        }
        else {
            int addr = (at - 1) * nCols;
            for (int j = 0; j < nCols; j++) {
                elems[addr] = elems[addr] + a[j];
                addr++;
            }
        }
        return this;
    }

    public Matrix addToRow(int at, RowVector a) {
        return addToRow(at, a.getArray());
    }

    public Matrix subtractFromRow(int at, double[] a) {
        if ((at < 1) || (at > mRows)) {
            throw newException("Attempt to manipulate row at off-index position.");
        }
        else if (a.length != nCols) {
            throw newException("Attempt to add row with incompatible size.");
        }
        else {
            int addr = (at - 1) * nCols;
            for (int j = 0; j < nCols; j++) {
                elems[addr] = elems[addr] - a[j];
                addr++;
            }
        }
        return this;
    }

    public Matrix subtractFromRow(int at, RowVector a) {
        return addToRow(at, a.getArray());
    }

    public Matrix multiplyColBy(int at, double a) {
        if ((at < 1) || (at > nCols)) {
            throw newException("Attempt to manipulate column at off-index position.");
        }
        else {
            int addr = at - 1;
            for (int i = 0; i < mRows; i++) {
                elems[addr] = a * elems[addr];
                addr += nCols;
            }
        }
        return this;
    }

    public Matrix divideColBy(int at, double a) {
        if ((at < 1) || (at > nCols)) {
            throw newException("Attempt to manipulate column at off-index position.");
        }
        else {
            int addr = at - 1;
            for (int i = 0; i < mRows; i++) {
                elems[addr] = elems[addr] / a;
                addr += nCols;
            }
        }
        return this;
    }

    public Matrix addToCol(int at, double a) {
        if ((at < 1) || (at > nCols)) {
            throw newException("Attempt to manipulate column at off-index position.");
        }
        else {
            int addr = at - 1;
            for (int i = 0; i < mRows; i++) {
                elems[addr] = elems[addr] + a;
                addr += nCols;
            }
        }
        return this;
    }

    public Matrix subtractFromCol(int at, double a) {
        if ((at < 1) || (at > nCols)) {
            throw newException("Attempt to manipulate column at off-index position.");
        }
        else {
            int addr = at - 1;
            for (int i = 0; i < mRows; i++) {
                elems[addr] = elems[addr] - a;
                addr += nCols;
            }
        }
        return this;
    }

    public Matrix negateCol(int at) {
        if ((at < 1) || (at > nCols)) {
            throw newException("Attempt to manipulate column at off-index position.");
        }
        else {
            int addr = at - 1;
            for (int i = 0; i < mRows; i++) {
                elems[addr] = -elems[addr];
                addr += nCols;
            }
        }
        return this;
    }

    public Matrix multiplyColBy(int at, double[] a) {
        if ((at < 1) || (at > nCols)) {
            throw newException("Attempt to manipulate column at off-index position.");
        }
        else {
            int addr = at - 1;
            for (int i = 0; i < mRows; i++) {
                elems[addr] = a[i] * elems[addr];
                addr += nCols;
            }
        }
        return this;
    }

    public Matrix multiplyColBy(int at, ColVector a) {
        return multiplyColBy(at, a.getArray());
    }

    public Matrix divideColBy(int at, double[] a) {
        if ((at < 1) || (at > nCols)) {
            throw newException("Attempt to manipulate column at off-index position.");
        }
        else {
            int addr = at - 1;
            for (int i = 0; i < mRows; i++) {
                elems[addr] = elems[addr] / a[i];
                addr += nCols;
            }
        }
        return this;
    }

    public Matrix divideColBy(int at, ColVector a) {
        return divideColBy(at, a.getArray());
    }

    public Matrix addToCol(int at, double[] a) {
        if ((at < 1) || (at > nCols)) {
            throw newException("Attempt to manipulate column at off-index position.");
        }
        else {
            int addr = at - 1;
            for (int i = 0; i < mRows; i++) {
                elems[addr] = elems[addr] + a[i];
                addr += nCols;
            }
        }
        return this;
    }

    public Matrix addToCol(int at, ColVector a) {
        return addToCol(at, a.getArray());
    }

    public Matrix subtractFromCol(int at, double[] a) {
        if ((at < 1) || (at > nCols)) {
            throw newException("Attempt to manipulate column at off-index position.");
        }
        else {
            int addr = at - 1;
            for (int i = 0; i < mRows; i++) {
                elems[addr] = elems[addr] - a[i];
                addr += nCols;
            }
        }
        return this;
    }

    public Matrix subtractFromCol(int at, ColVector a) {
        return subtractFromCol(at, a.getArray());
    }

    public Matrix multiplyBy(Matrix A) {
        if ((A.mRows != mRows) || (A.nCols != nCols)) {
            throw newException("Attempt to manipulate matrix with another matrix of incompatible size.");
        }
        else {
            double[] a = A.getArray();
            for (int i = 0; i < size; i++) {
                elems[i] = elems[i] * a[i];
            }
        }
        return this;
    }

    public Matrix divideBy(Matrix A) {
        if ((A.mRows != mRows) || (A.nCols != nCols)) {
            throw newException("Attempt to manipulate matrix with another matrix of incompatible size.");
        }
        else {
            double[] a = A.getArray();
            for (int i = 0; i < size; i++) {
                elems[i] = elems[i] / a[i];
            }
        }
        return this;
    }

    public Matrix add(Matrix A) {
        if ((A.mRows != mRows) || (A.nCols != nCols)) {
            throw newException("Attempt to manipulate matrix with another matrix of incompatible size.");
        }
        else {
            double[] a = A.getArray();
            for (int i = 0; i < size; i++) {
                elems[i] = elems[i] + a[i];
            }
        }
        return this;
    }

    public Matrix subtract(Matrix A) {
        if ((A.mRows != mRows) || (A.nCols != nCols)) {
            throw newException("Attempt to manipulate matrix with another matrix of incompatible size.");
        }
        else {
            double[] a = A.getArray();
            for (int i = 0; i < size; i++) {
                elems[i] = elems[i] - a[i];
            }
        }
        return this;
    }

    public Matrix equalizeTo(Matrix B) {
        if ((nCols != B.nCols) || (mRows != B.mRows)) {
            throw newException("Attempt to equalize to a matrix of incompatible dimensions.");
        }
        double[] elemsA = getArray();
        double[] elemsB = B.getArray();
        for (int i = 0; i < size; i++) {
            elemsA[i] = elemsB[i];
        }
        return this;
    }

    public boolean equals(Matrix B) {
        boolean eq = false;
        if ((nCols != B.nCols) || (mRows != B.mRows)) {
            throw newException("Attempt to compare with a matrix of incompatible dimensions.");
        }
        else {
            double[] elemsB = B.getArray();
            for (int i = 0; i < size; i++) {
                if (elems[i] != elemsB[i]) {
                    eq = false;
                    break;
                }
            }
        }
        return eq;
    }

    public boolean equalsRow(int at, double[] row) {
        boolean eq = false;
        if ((at < 1) || (at > mRows)) {
            throw newException("Attempt to compare a row at off-index position.");
        }
        else if (row.length != nCols) {
            throw newException("Attempt to compare with a row of incompatible dimensions.");
        }
        else {
            int addr = (at - 1) * nCols;
            for (int j = 0; j < nCols; j++) {
                if (elems[addr] != row[j]) {
                    eq = false;
                    break;
                }
                addr++;
            }
        }
        return eq;
    }

    public boolean equalsRow(int at, RowVector row) {
        return equalsRow(at, row.getArray());
    }

    public boolean equalsCol(int at, double[] col) {
        boolean eq = false;
        if ((at < 1) || (at > nCols)) {
            throw newException("Attempt to compare a column at off-index position.");
        }
        else if (col.length != mRows) {
            throw newException("Attempt to compare with a column of incompatible dimensions.");
        }
        else {
            int addr = 0;
            for (int i = 0; i < mRows; i++) {
                if (elems[addr] != col[i]) {
                    eq = false;
                    break;
                }
                addr += nCols;
            }
        }
        return eq;
    }

    public boolean equalsCol(int at, ColVector col) {
        return equalsCol(at, col.getArray());
    }

    public int addrOfMax() {
        return doFindMax(elems);
    }

    public Point posOfMax() {
        return pos(addrOfMax());
    }

    public double maxElement() {
        return elems[addrOfMax()];
    }

    public int addrOfMin() {
        return doFindMin(elems);
    }

    public Point posOfMin() {
        return pos(addrOfMin());
    }

    public double minElement() {
        return elems[addrOfMin()];
    }

    public int addrOfMaxAbs() {
        return doFindMaxAbs(elems);
    }

    public Point posOfMaxAbs() {
        return pos(addrOfMaxAbs());
    }

    public double maxAbsElement() {
        return elems[addrOfMaxAbs()];
    }

    public int addrOfMinAbs() {
        return doFindMinAbs(elems);
    }

    public Point posOfMinAbs() {
        return pos(addrOfMinAbs());
    }

    public double minAbsElement() {
        return elems[addrOfMinAbs()];
    }

    public int addrOfMaxInRow(int at) {
        return doFindMax(getRow(at));
    }

    public int indexOfMaxInRow(int at) {
        return doFindMax(getRow(at)) + 1;
    }

    public double maxElementInRow(int at) {
        double[] row = getRow(at);
        return row[doFindMax(row)];
    }

    public int addrOfMinInRow(int at) {
        return doFindMin(getRow(at));
    }

    public int indexOfMinInRow(int at) {
        return doFindMin(getRow(at)) + 1;
    }

    public double minElementInRow(int at) {
        double[] row = getRow(at);
        return row[doFindMin(row)];
    }

    public int addrOfMaxAbsInRow(int at) {
        return doFindMaxAbs(getRow(at));
    }

    public int indexOfMaxAbsInRow(int at) {
        return doFindMaxAbs(getRow(at)) + 1;
    }

    public double maxAbsElementInRow(int at) {
        double[] row = getRow(at);
        return row[doFindMaxAbs(row)];
    }

    public int addrOfMinAbsInRow(int at) {
        return doFindMinAbs(getRow(at));
    }

    public int indexOfMinAbsInRow(int at) {
        return doFindMinAbs(getRow(at)) + 1;
    }

    public double minAbsElementInRow(int at) {
        double[] row = getRow(at);
        return row[doFindMinAbs(row)];
    }

    public int addrOfMaxInCol(int at) {
        return doFindMax(getCol(at));
    }

    public int indexOfMaxInCol(int at) {
        return doFindMax(getCol(at)) + 1;
    }

    public double maxElementInCol(int at) {
        double[] col = getCol(at);
        return col[doFindMax(col)];
    }

    public int addrOfMinInCol(int at) {
        return doFindMin(getCol(at));
    }

    public int indexOfMinInCol(int at) {
        return doFindMin(getCol(at)) + 1;
    }

    public double minElementInCol(int at) {
        double[] col = getCol(at);
        return col[doFindMin(col)];
    }

    public int addrOfMaxAbsInCol(int at) {
        return doFindMaxAbs(getCol(at));
    }

    public int indexOfMaxAbsInCol(int at) {
        return doFindMaxAbs(getCol(at)) + 1;
    }

    public double maxAbsElementInCol(int at) {
        double[] col = getCol(at);
        return col[doFindMaxAbs(col)];
    }

    public int addrOfMinAbsInCol(int at) {
        return doFindMinAbs(getCol(at));
    }

    public int indexOfMinAbsInCol(int at) {
        return doFindMinAbs(getCol(at)) + 1;
    }

    public double minAbsElementInCol(int at) {
        double[] col = getCol(at);
        return col[doFindMinAbs(col)];
    }

    // --- Algebra operations returning a new matrix

    public Matrix transposed() {
        Matrix A = createClone();
        double[] newElems = A.getArray();
        doTranspose(newElems);
        return A;
    }

    public Matrix inverted() {
        if (nCols != mRows) {
            throw newException("Cannot invert a non-square matrix.");
        }
        else {
            Matrix A = createClone();
            SLE.solve(A, Matrix.Unit(nCols));
            return A;
        }
    }

    public Matrix times(double a) {
        Matrix A = createClone();
        double[] newElems = A.getArray();
        doMultiply(newElems, a);
        A.setArray(newElems, mRows, nCols);
        return A;
    }

    public Matrix by(double a) {
        Matrix A = createClone();
        double[] newElems = A.getArray();
        doDivide(newElems, a);
        A.setArray(newElems, mRows, nCols);
        return A;
    }

    public Matrix plus(double a) {
        Matrix A = createClone();
        double[] newElems = A.getArray();
        doAdd(newElems, a);
        A.setArray(newElems, mRows, nCols);
        return A;
    }

    public Matrix minus(double a) {
        Matrix A = createClone();
        double[] newElems = A.getArray();
        doSubstract(newElems, a);
        A.setArray(newElems, mRows, nCols);
        return A;
    }

    public Matrix negative(double a) {
        Matrix A = createClone();
        double[] newElems = A.getArray();
        doNegate(newElems);
        A.setArray(newElems, mRows, nCols);
        return A;
    }

    public Matrix times(Matrix B) {
        Matrix C = null;
        if (nCols != B.mRows) {
            throw newException("Attempt to multiply matrices with incompatible dimensions.");
        }
        else {
            C = new Matrix(mRows, B.nCols);
            double[] elemsB = B.getArray();
            double[] elemsC = C.getArray();
            double sum = 0;
            int addrC = 0;
            int startA = 0;
            int addrA = 0;
            int addrB = 0;
            for (int i = 0; i < mRows; i++) {
                for (int j = 0; j < B.nCols; j++) {
                    addrA = startA;
                    addrB = j;
                    sum = 0;
                    for (int k = 0; k < nCols; k++) {
                        sum += elems[addrA] * elemsB[addrB];
                        addrA++;
                        addrB += B.nCols;
                    }
                    elemsC[addrC] = sum;
                    addrC++;

                }
                startA += nCols;
            }
        }
        return C;
    }

    // --- Implementation of Cloneable

    @Override
    public Object clone() {
        Matrix clone = new Matrix(mRows, nCols);
        clone.setArray(elems.clone(), mRows, nCols);
        return clone;
    }

    public Matrix createClone() {
        return (Matrix) clone();
    }

}
