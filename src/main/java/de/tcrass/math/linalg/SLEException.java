package de.tcrass.math.linalg;

import de.tcrass.math.MathException;

public class SLEException extends MathException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Matrix            A;
    private Matrix            B;

    public SLEException(Matrix A, Matrix B, String msg) {
        super(msg);
        this.A = A;
        this.B = B;
    }

    public Matrix getCoefficientMatrix() {
        return A;
    }

    public Matrix getRightHandSides() {
        return B;
    }

}
