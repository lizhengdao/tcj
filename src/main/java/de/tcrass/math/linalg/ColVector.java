package de.tcrass.math.linalg;

public class ColVector extends Matrix implements FloatVector {

    public ColVector(int rows) {
        super(rows, 1);
    }

    public ColVector(double[] elems) {
        this(elems.length);
        setArray(elems.clone(), elems.length, 1);
    }

    @Override
    public Matrix transpose() {
        throw newException("transpose() not supported by ColVector; use transposed() instead");
    }

    @Override
    public Matrix transposed() {
        return new RowVector(super.transpose().getArray());
    }

    // --- Implementation of FloatVector

    public double length() {
        return mRows;
    }

    public double[] elems() {
        return getCol(1);
    }

}
