package de.tcrass.math.data;

import java.util.ArrayList;
import java.util.List;

public class Data extends TabularImplStub {

    protected ArrayList<Object>[] data;

    protected static Class<?>[] getClassArray(Class<?> clazz, int n) {
        Class<?>[] types = new Class[n];
        for (int i = 0; i < n; i++) {
            types[i] = clazz;
        }
        return types;
    }

    public static Data intData() {
        return intData("X", "Y");
    }

    public static Data intData(String... colNames) {
        return intData(Tabular.MIN_CAPACITY, colNames);
    }

    public static Data intData(int cap, String... colNames) {
        return new Data(getClassArray(Long.class, colNames.length), colNames, cap);
    }

    public static Data floatData() {
        return floatData("X", "Y");
    }

    public static Data floatData(String... colNames) {
        return floatData(Tabular.MIN_CAPACITY, colNames);
    }

    public static Data floatData(int cap, String... colNames) {
        return new Data(getClassArray(Double.class, colNames.length), colNames, cap);
    }

    // --- Constructor ---

    @SuppressWarnings("unchecked")
    protected Data(Class<?>[] types, String[] names, int cap) {
        super(types, names);
        data = new ArrayList[types.length];
        for (int i = 0; i < types.length; i++) {
            data[i] = createCol(cap);
        }
    }

    // --- Protected methods ---

    protected static ArrayList<Object> createCol(int cap) {
        return new ArrayList<Object>(cap);
    }

    protected List<Object> assertRow(int col, int row) {
        if (data[col].size() <= row) {
            for (ArrayList<Object> element : data) {
                element.ensureCapacity(row + 1);
                while (element.size() <= row) {
                    element.add(null);
                }
            }
        }
        return data[col];
    }

    // --- Public methods ---

    public long getInt(int col, int row) {
        Object o = getData(col, row);
        if (o == null) {
            return 0;
        }
        else {
            return ((Long) o).longValue();
        }
    }

    public long getInt(String colName, int row) {
        return ((Long) getInt(names2cols.get(colName), row)).longValue();
    }

    public void setInt(int col, int row, long d) {
        setData(col, row, Long.valueOf(d));
    }

    public void setInt(String colName, int row, long d) {
        setData(colName, row, Long.valueOf(d));
    }

    public double getFloat(int col, int row) {
        Object o = getData(col, row);
        if (o == null) {
            return 0;
        }
        else {
            return ((Double) o).doubleValue();
        }
    }

    public double getFloat(String colName, int row) {
        return ((Double) getFloat(names2cols.get(colName), row)).doubleValue();
    }

    public void setFloat(int col, int row, double d) {
        setData(col, row, Double.valueOf(d));
    }

    public void setFloat(String colName, int row, double d) {
        setData(colName, row, Double.valueOf(d));
    }

    // --- Implementation of Tabular

    public int mRows() {
        return data[0].size();
    }

    public List<Object> getCol(int col) {
        return data[col];
    }

    public List<Object> getCol(String colName) {
        return data[names2cols.get(colName)];
    }

    public Object getData(int col, int row) {
        return assertRow(col, row).get(row);
    }

    public Object getData(String colName, int row) {
        return assertRow(names2cols.get(colName), row).get(row);
    }

    public void setData(int col, int row, Object d) {
        if (colTypes[col].isInstance(d)) {
            assertRow(col, row).set(row, d);
        }
    }

    public void setData(String colName, int row, Object d) {
        setData(names2cols.get(colName), row, d);
    }

    public List<Object> getRow(int row) {
        List<Object> rowData = new ArrayList<Object>(data.length);
        for (int i = 0; i < data.length; i++) {
            rowData.set(i, data[i].get(row));
        }
        return rowData;
    }

}
