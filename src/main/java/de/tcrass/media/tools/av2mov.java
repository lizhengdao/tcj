/*
 * Created on 11.01.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.media.tools;

import java.io.File;
import java.util.Vector;

import de.tcrass.media.conv.AudioVideoToQuicktimeMuxer;
import de.tcrass.util.SysUtils;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class av2mov {

    private static String outputFilename = null;
    private static String audioFilename  = null;
    private static String videoFilename  = null;
    private static Vector sourcesURLs    = new Vector(1);

    public static void main(String[] args) {
        int i = 0;
        if (args.length == 0) {
            printUsage();
            System.exit(0);
        }
        while (i < args.length) {
            if (args[i].equals("-v")) {
                i++;
                videoFilename = args[i];
            }
            else if (args[i].equals("-a")) {
                i++;
                audioFilename = args[i];
            }
            else {
                outputFilename = args[i];
            }
            i++;
        }

        if ((audioFilename == null) || (videoFilename == null)) {
            printUsage();
            System.exit(0);
        }
        if (outputFilename == null) {
            outputFilename = "movie.mov";
        }
        if (!outputFilename.toLowerCase().endsWith(".mov")) {
            outputFilename = outputFilename + ".mov";
        }

        File audioFile = new File(audioFilename);
        File videoFile = new File(videoFilename);
        File outputFile = new File(outputFilename);

        AudioVideoToQuicktimeMuxer mux = new AudioVideoToQuicktimeMuxer();
        try {
            mux.multiplex(audioFile, videoFile, outputFile);
        }
        catch (Exception e) {
            SysUtils.reportException(e);
            printUsage();
            System.exit(1);
        }
        System.exit(0);
    }

    private static void printUsage() {
        System.err.println("Usage: av2mov -a <audio_file> -v <video_file> [<output_filename>]");
    }

}
