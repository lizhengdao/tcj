package de.tcrass.media.gst;

import org.gstreamer.Gst;


public class GSTMediaManager {

    private static boolean initialized = false;
    
    public static void init(String[] args) {
        if (!initialized) {
            Gst.init("GStreamer Media Manager", args);
            initialized = true;
        }
    }

    public static void init() {
        init(new String[] {});
    }
    
}
