/*
 * Created on 15.03.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.media.conv;

import javax.media.Buffer;
import javax.media.Control;
import javax.media.Format;
import javax.media.protocol.BufferTransferHandler;
import javax.media.protocol.ContentDescriptor;
import javax.media.protocol.PushBufferStream;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class SourceStream implements PushBufferStream, BufferTransferHandler {

    PushBufferStream      sourceStream;
    BufferTransferHandler handler;

    public SourceStream(StreamDataSource s) {
        sourceStream = s.getSourceStream();
        sourceStream.setTransferHandler(this);
    }

    public void read(Buffer buf) {
    }

    public ContentDescriptor getContentDescriptor() {
        return new ContentDescriptor(ContentDescriptor.RAW);
    }

    public boolean endOfStream() {
        return sourceStream.endOfStream();
    }

    public long getContentLength() {
        return LENGTH_UNKNOWN;
    }

    public Format getFormat() {
        return sourceStream.getFormat();
    }

    public void setTransferHandler(BufferTransferHandler h) {
        handler = h;
    }

    public Object getControl(String name) {
        return null;
    }

    public Object[] getControls() {
        return new Control[0];
    }

    public synchronized void transferData(PushBufferStream pbs) {
        if (handler != null) {
            handler.transferData(pbs);
        }
    }

}
