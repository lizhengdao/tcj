/*
 * Created on 15.03.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.media.conv;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.media.DataSink;
import javax.media.Manager;
import javax.media.MediaLocator;
import javax.media.NoDataSinkException;
import javax.media.datasink.DataSinkErrorEvent;
import javax.media.datasink.DataSinkEvent;
import javax.media.datasink.DataSinkListener;
import javax.media.datasink.EndOfStreamEvent;
import javax.media.protocol.DataSource;

import de.tcrass.util.SysUtils;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class MediaFileWriter implements DataSinkListener {

    private DataSink sink;

    private boolean  done    = false;
    private Object   runLock = new Object();

    public MediaFileWriter(DataSource source, File f) throws NoDataSinkException, IOException {
        URL url = null;
        try {
            url = f.toURL();
        }
        catch (MalformedURLException ex) {
            throw new IOException(ex.getMessage());
        }
        MediaLocator ml = new MediaLocator(url);
        sink = Manager.createDataSink(source, new MediaLocator(f.toURL()));
        sink.addDataSinkListener(this);
        sink.open();
        sink.start();
    }

    // --- Private methods ---

    private void waitForDone() {
        synchronized (runLock) {
            while (!done) {
                try {
                    runLock.wait();
                }
                catch (InterruptedException ex) {
                    SysUtils.doNothing();
                }
            }
        }
    }

    // --- Public methods ---

    public void close() {
        waitForDone();
        sink.close();
    }

    // --- Implementation of DataSinkListener

    public void dataSinkUpdate(DataSinkEvent e) {
        synchronized (runLock) {
            if (e instanceof EndOfStreamEvent) {
                sink.close();
                done = true;
            }
            else if (e instanceof DataSinkErrorEvent) {
                sink.close();
                done = true;
            }
            runLock.notifyAll();
        }
    }

}
