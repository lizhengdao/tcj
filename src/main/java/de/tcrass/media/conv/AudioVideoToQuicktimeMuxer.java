package de.tcrass.media.conv;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.media.ControllerEvent;
import javax.media.ControllerListener;
import javax.media.EndOfMediaEvent;
import javax.media.Manager;
import javax.media.MediaException;
import javax.media.MediaLocator;
import javax.media.Processor;
import javax.media.control.TrackControl;
import javax.media.format.AudioFormat;
import javax.media.format.VideoFormat;
import javax.media.protocol.ContentDescriptor;
import javax.media.protocol.DataSource;

import de.tcrass.media.jmf.MediaUtils;
import de.tcrass.util.SysUtils;

public class AudioVideoToQuicktimeMuxer implements ControllerListener {

    private boolean running = false;
    private Object  runLock = new Object();

    // --- Constructor ---

    public AudioVideoToQuicktimeMuxer() {
    }

    // --- Public methods ---

    public void multiplex(File audioFile, File videoFile, File outputFile)
    throws MediaException, IOException {

        synchronized (runLock) {
            while (running) {
                try {
                    runLock.wait();
                }
                catch (InterruptedException ex) {
                    SysUtils.doNothing();
                }
            }
            running = true;

            Vector sourceURLs;
            sourceURLs = new Vector(1);
            String outURL = null;

            // sourceURLs.add("file:" + audioFile.getPath());
            sourceURLs.add(audioFile.getCanonicalFile().toURL().toExternalForm());
            outURL = outputFile.getCanonicalFile().toURL().toExternalForm();
            // sourcesURLs.add(audioFile.getCanonicalFile().toURL().
            // toExternalForm());
            sourceURLs.add(videoFile.getCanonicalFile().toURL().toExternalForm());

            Processor[] any2rawProcs = new Processor[sourceURLs.size()];
            DataSource[] rawSources = new DataSource[sourceURLs.size()];

            for (int i = 0; i < sourceURLs.size(); i++) {
                String sourceURL = (String) sourceURLs.get(i);
                MediaLocator ml = new MediaLocator(sourceURL);
                any2rawProcs[i] = Manager.createProcessor(ml);
                MediaUtils.configure(any2rawProcs[i]);
                any2rawProcs[i].setContentDescriptor(new ContentDescriptor(
                    ContentDescriptor.RAW));

                // TrackControl[] tracks =
                // any2rawProcs[i].getTrackControls();
                // for (int j = 0; j < tracks.length; j++) {
                // if (tracks[j].getFormat() instanceof AudioFormat) {
                // tracks[j].setCodecChain(
                // new Codec[] {
                // new com.omnividea.media.codec.video.NativeDecoder()
                // }
                // );
                // }
                // }

                MediaUtils.realize(any2rawProcs[i]);
                rawSources[i] = any2rawProcs[i].getDataOutput();

                any2rawProcs[i].start();
            }

            DataSource mergedSource = Manager.createMergingDataSource(rawSources);
            if (mergedSource == null) {
                throw new MediaException();
            }
            mergedSource.start();

            Processor raw2encProc = Manager.createProcessor(mergedSource);
            MediaUtils.configure(raw2encProc);
            raw2encProc.setContentDescriptor(new ContentDescriptor("video.quicktime"));
            TrackControl[] tracks = raw2encProc.getTrackControls();
            for (TrackControl element : tracks) {
                if (element.getFormat() instanceof VideoFormat) {
                    VideoFormat sourceVideoFormat = (VideoFormat) element.getFormat();
                    VideoFormat targetVideoFormat = (VideoFormat) sourceVideoFormat.clone();
                    element.setFormat(targetVideoFormat);
                }
                else if (element.getFormat() instanceof AudioFormat) {
                    AudioFormat sourceAudioFormat = (AudioFormat) element.getFormat();
                    System.out.println(sourceAudioFormat.toString());
                    AudioFormat targetAudioFormat = (AudioFormat) sourceAudioFormat.clone();
                    element.setFormat(targetAudioFormat);
                }
            }
            MediaUtils.realize(raw2encProc);
            raw2encProc.addControllerListener(this);
            raw2encProc.start();

            MediaFileWriter writer = new MediaFileWriter(raw2encProc.getDataOutput(),
                outputFile);

            while (running) {
                try {
                    runLock.wait();
                }
                catch (InterruptedException ex) {
                    SysUtils.doNothing();
                }
            }

            writer.close();
        }
    }

    // --- Implementation of ControllerListener

    public synchronized void controllerUpdate(ControllerEvent e) {
        Processor p = (Processor) e.getSource();
        if (e instanceof EndOfMediaEvent) {
            p.close();
            synchronized (runLock) {
                running = false;
                runLock.notifyAll();
            }
        }
    }

    // //--- Implementation of DataSinkListener
    //  
    // public void dataSinkUpdate(DataSinkEvent e) {
    // DataSink s = (DataSink)e.getSource();
    // if (e instanceof EndOfStreamEvent) {
    // synchronized(runLock) {
    // running = false;
    // runLock.notifyAll();
    // }
    // }
    // else if (e instanceof DataSinkErrorEvent) {
    // synchronized(runLock) {
    // running = false;
    // runLock.notifyAll();
    // }
    // }
    // }

}
