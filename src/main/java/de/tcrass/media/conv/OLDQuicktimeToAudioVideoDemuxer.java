/*
 * Based on SUN's JpegToQuicktime JFM demonstration program
 */
// ---------------------------------------------------------
/*
 * @(#)Split.java 1.4 01/03/13 Copyright (c) 1999-2001 Sun Microsystems,
 * Inc. All Rights Reserved. Sun grants you ("Licensee") a
 * non-exclusive, royalty free, license to use, modify and redistribute
 * this software in source and binary code form, provided that i) this
 * copyright notice and license appear on all copies of the software;
 * and ii) Licensee does not utilize the software in a manner which is
 * disparaging to Sun. This software is provided "AS IS," without a
 * warranty of any kind. ALL EXPRESS OR IMPLIED CONDITIONS,
 * REPRESENTATIONS AND WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN AND ITS LICENSORS SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THE SOFTWARE OR ITS DERIVATIVES. IN
 * NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR ANY LOST REVENUE,
 * PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL,
 * INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE
 * THEORY OF LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE
 * SOFTWARE, EVEN IF SUN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGES. This software is not designed or intended for use in on-line
 * control of aircraft, air traffic, aircraft navigation or aircraft
 * communications; or in the design, construction, operation or
 * maintenance of any nuclear facility. Licensee represents and warrants
 * that it will not use or redistribute the Software for such purposes.
 */

package de.tcrass.media.conv;

// import java.io.File;
// import javax.media.*;
// import javax.media.control.TrackControl;
// import javax.media.control.QualityControl;
// import javax.media.Format;
// import javax.media.format.*;
// import javax.media.datasink.*;
// import javax.media.protocol.*;
// import javax.media.protocol.DataSource;
// import java.io.IOException;
//
// import org.comlink.home.tcrass.io.*;
//
// /**
// * A sample program to split an input media file with multiplexed
// * audio and video tracks into files of individual elementary track.
// */
//
public class OLDQuicktimeToAudioVideoDemuxer {
    //  
    // private SplitDataSource splitDS[];
    // private Object fileSync = new Object();
    // private boolean allDone = false;
    //  
    // private File audioFile;
    // private int audioFiles = 0;
    // private File videoFile;
    // private int videoFiles = 0;
    //  
    // /**
    // * Main program
    // */
    // public void demux(File inputFile, File audioFile, File videoFile)
    // throws MediaException, IOException {
    //    
    // String inputURL = "file:" + inputFile.getPath();
    // MediaLocator iml = new MediaLocator(inputURL);
    //
    // this.audioFile = audioFile;
    // this.videoFile = videoFile;
    //    
    // doIt(iml);
    //    
    // }
    //  
    //  
    // /**
    // * Splits the tracks from a multiplexed input.
    // */
    // private boolean doIt(MediaLocator inML)
    // throws MediaException, IOException {
    //    
    // Processor p;
    //    
    // p = Manager.createProcessor(inML);
    //    
    // waitForState(p, Processor.Configured);
    //    
    // // If the input is an MPEG file, we'll first convert that to
    // // raw audio and video.
    // if
    //(FileTypeDescriptor.MPEG.equals(fileExtToCD(FileUtils.getExtension
    // (inML.toExternalForm())).getEncoding()))
    // {
    // transcodeMPEGToRaw(p);
    // }
    //    
    // waitForState(p, Processor.Realized);
    //    
    // // Set the JPEG quality to .5.
    // setJPEGQuality(p, 1f);
    //    
    // // Get the output data streams from the first processor.
    // // Create a SplitDataSource for each of these elementary stream.
    // PushBufferDataSource pbds =
    // (PushBufferDataSource)p.getDataOutput();
    // PushBufferStream pbs[] = pbds.getStreams();
    // splitDS = new SplitDataSource[pbs.length];
    //    
    // allDone = false;
    // boolean atLeastOne = false;
    //    
    // // Create a file writer for each SplitDataSource to generate
    // // the resulting media file.
    // for (int i = 0; i < pbs.length; i++) {
    // splitDS[i] = new SplitDataSource(p, i);
    // if ((new FileWriter()).write(splitDS[i]))
    // atLeastOne = true;
    // }
    //    
    // if (!atLeastOne) {
    // throw new MediaException("No tracks found!");
    // }
    //    
    // waitForFileDone();
    //    
    // return true;
    // }
    //  
    //  
    // /**
    // * Callback from the FileWriter when a DataSource is done.
    // */
    // void doneFile() {
    // synchronized (fileSync) {
    // for (int i = 0; i < splitDS.length; i++) {
    // if (!splitDS[i].done) {
    // return;
    // }
    // }
    //      
    // // All done.
    // allDone = true;
    // fileSync.notify();
    // }
    // }
    //  
    //  
    // void waitForFileDone() {
    // synchronized (fileSync) {
    // while (!allDone) {
    // try {
    // fileSync.wait(1000);
    // } catch (Exception e) {}
    // }
    // }
    // }
    //  
    //  
    // /**
    // * Transcode the MPEG audio to linear and video to JPEG so
    // * we can do the splitting.
    // */
    // void transcodeMPEGToRaw(Processor p) {
    //    
    // TrackControl tc[] = p.getTrackControls();
    // AudioFormat afmt;
    //    
    // for (int i = 0; i < tc.length; i++) {
    // if (tc[i].getFormat() instanceof VideoFormat)
    // tc[i].setFormat(new VideoFormat(VideoFormat.JPEG));
    // else if (tc[i].getFormat() instanceof AudioFormat) {
    // afmt = (AudioFormat)tc[i].getFormat();
    // tc[i].setFormat(new AudioFormat(AudioFormat.LINEAR,
    // afmt.getSampleRate(),
    // afmt.getSampleSizeInBits(),
    // afmt.getChannels()));
    // }
    // }
    // }
    //  
    //  
    // /**
    // * Setting the encoding quality to the specified value on the JPEG
    // encoder.
    // * 0.5 is a good default.
    // */
    // void setJPEGQuality(Player p, float val) {
    //    
    // Control cs[] = p.getControls();
    // QualityControl qc = null;
    // VideoFormat jpegFmt = new VideoFormat(VideoFormat.JPEG);
    //    
    // // Loop through the controls to find the Quality control for
    // // the JPEG encoder.
    // for (int i = 0; i < cs.length; i++) {
    //      
    // if (cs[i] instanceof QualityControl &&
    // cs[i] instanceof Owned) {
    // Object owner = ((Owned)cs[i]).getOwner();
    //        
    // // Check to see if the owner is a Codec.
    // // Then check for the output format.
    // if (owner instanceof Codec) {
    // Format fmts[] = ((Codec)owner).getSupportedOutputFormats(null);
    // for (int j = 0; j < fmts.length; j++) {
    // if (fmts[j].matches(jpegFmt)) {
    // qc = (QualityControl)cs[i];
    // qc.setQuality(val);
    // break;
    // }
    // }
    // }
    // if (qc != null)
    // break;
    // }
    // }
    // }
    //  
    //  
    // /**
    // * Utility class to block until a certain state had reached.
    // */
    // public class StateWaiter implements ControllerListener {
    //    
    // Processor p;
    // boolean error = false;
    //    
    // StateWaiter(Processor p) {
    // this.p = p;
    // p.addControllerListener(this);
    // }
    //    
    // public synchronized boolean waitForState(int state) {
    //      
    // switch (state) {
    // case Processor.Configured:
    // p.configure(); break;
    // case Processor.Realized:
    // p.realize(); break;
    // case Processor.Prefetched:
    // p.prefetch(); break;
    // case Processor.Started:
    // p.start(); break;
    // }
    //      
    // while (p.getState() < state && !error) {
    // try {
    // wait(1000);
    // } catch (Exception e) {
    // }
    // }
    // //p.removeControllerListener(this);
    // return !(error);
    // }
    //    
    // public void controllerUpdate(ControllerEvent ce) {
    // if (ce instanceof ControllerErrorEvent) {
    // error = true;
    // }
    // synchronized (this) {
    // notifyAll();
    // }
    // }
    // }
    //  
    //  
    // /**
    // * Block until the given processor has transitioned to the given
    // state.
    // * Return false if the transition failed.
    // */
    // boolean waitForState(Processor p, int state) {
    // return (new StateWaiter(p)).waitForState(state);
    // }
    //  
    //  
    // /**
    // * Convert a file name to a content type. The extension is parsed
    // * to determine the content type.
    // */
    // ContentDescriptor fileExtToCD(String ext) {
    //    
    // ext = ext.toLowerCase();
    //    
    // String type;
    //    
    // // Use the MimeManager to get the mime type from the file
    // extension.
    // if ( ext.equals("mp3")) {
    // type = FileTypeDescriptor.MPEG_AUDIO;
    // } else {
    // if ((type = com.sun.media.MimeManager.getMimeType(ext)) == null)
    // return null;
    // type = ContentDescriptor.mimeTypeToPackageName(type);
    // }
    //
    // return new FileTypeDescriptor(type);
    // }
    //  
    //  
    // /**
    // * Create a media locator from the given string.
    // */
    // static MediaLocator createMediaLocator(String url) {
    //    
    // MediaLocator ml;
    //    
    // if (url.indexOf(":") > 0 && (ml = new MediaLocator(url)) != null)
    // return ml;
    //    
    // if (url.startsWith(File.separator)) {
    // if ((ml = new MediaLocator("file:" + url)) != null)
    // return ml;
    // } else {
    // String file = "file:" + System.getProperty("user.dir") +
    // File.separator + url;
    // if ((ml = new MediaLocator(file)) != null)
    // return ml;
    // }
    //    
    // return null;
    // }
    //  
    //  
    // ////////////////////////////////////////
    // //
    // // Inner classes.
    // ////////////////////////////////////////
    //  
    // /**
    // * The custom DataSource to split input.
    // */
    // class SplitDataSource extends PushBufferDataSource {
    //    
    // Processor p;
    // PushBufferDataSource ds;
    // PushBufferStream pbs[];
    // SplitStream streams[];
    // int idx;
    // boolean done = false;
    //    
    // public SplitDataSource(Processor p, int idx) {
    // this.p = p;
    // this.ds = (PushBufferDataSource)p.getDataOutput();
    // this.idx = idx;
    // pbs = ds.getStreams();
    // streams = new SplitStream[1];
    // streams[0] = new SplitStream(pbs[idx]);
    // }
    //    
    // public void connect() throws java.io.IOException {
    // }
    //    
    // public PushBufferStream [] getStreams() {
    // return streams;
    // }
    //    
    // public Format getStreamFormat() {
    // return pbs[idx].getFormat();
    // }
    //    
    // public void start() throws java.io.IOException {
    // p.start();
    // ds.start();
    // }
    //    
    // public void stop() throws java.io.IOException {
    // }
    //    
    // public Object getControl(String name) {
    // // No controls
    // return null;
    // }
    //    
    // public Object [] getControls() {
    // // No controls
    // return new Control[0];
    // }
    //    
    // public Time getDuration() {
    // return ds.getDuration();
    // }
    //    
    // public void disconnect() {
    // }
    //    
    // public String getContentType() {
    // return ContentDescriptor.RAW;
    // }
    //    
    // public MediaLocator getLocator() {
    // return ds.getLocator();
    // }
    //    
    // public void setLocator(MediaLocator ml) {
    // }
    // }
    //  
    //  
    // /**
    // * Utility Source stream for the SplitDataSource.
    // */
    // class SplitStream implements PushBufferStream,
    // BufferTransferHandler {
    //    
    // PushBufferStream pbs;
    //    
    // BufferTransferHandler bth;
    // Format format;
    //    
    // public SplitStream(PushBufferStream pbs) {
    // this.pbs = pbs;
    // pbs.setTransferHandler(this);
    // }
    //    
    // public void read(Buffer buf) /* throws IOException */{
    // // This wouldn't be used.
    // }
    //    
    // public ContentDescriptor getContentDescriptor() {
    // return new ContentDescriptor(ContentDescriptor.RAW);
    // }
    //    
    // public boolean endOfStream() {
    // return pbs.endOfStream();
    // }
    //    
    // public long getContentLength() {
    // return LENGTH_UNKNOWN;
    // }
    //    
    // public Format getFormat() {
    // return pbs.getFormat();
    // }
    //    
    // public void setTransferHandler(BufferTransferHandler bth) {
    // this.bth = bth;
    // }
    //    
    // public Object getControl(String name) {
    // // No controls
    // return null;
    // }
    //    
    // public Object [] getControls() {
    // // No controls
    // return new Control[0];
    // }
    //    
    // public synchronized void transferData(PushBufferStream pbs) {
    // if (bth != null)
    // bth.transferData(pbs);
    // }
    //    
    // } // class SplitStream
    //  
    //  
    // /**
    // * Given a DataSource, creates a DataSink and generate a file.
    // */
    // class FileWriter implements ControllerListener, DataSinkListener
    // {
    //    
    // Processor p;
    // SplitDataSource ds;
    // DataSink dsink;
    //    
    // boolean write(SplitDataSource ds)
    // throws MediaException, IOException {
    //      
    // this.ds = ds;
    //      
    // // Create the processor to generate the final output.
    // p = Manager.createProcessor(ds);
    //      
    // p.addControllerListener(this);
    //      
    // // Put the Processor into configured state.
    // waitForState(p, Processor.Configured);
    //      
    // File f;
    // int n;
    // if (ds.getStreamFormat() instanceof AudioFormat) {
    // f = audioFile;
    // n = audioFiles;
    // audioFiles++;
    // } else {
    // f = videoFile;
    // n = videoFiles;
    // videoFiles++;
    // }
    //      
    // String filename = f.getParent();
    // if (filename == null) {
    // filename = "";
    // }
    // else {
    // filename = filename + File.separator;
    // }
    // filename = filename + FileUtils.getNameWithoutExtension(f);
    // if (n > 0) {
    // filename = filename + n;
    // }
    // filename = filename + "." + FileUtils.getExtension(f);
    //      
    // ContentDescriptor cd;
    // if ((cd = fileExtToCD(FileUtils.getExtension(f))) == null) {
    // throw new MediaException("Unknown output file type!");
    // }
    //      
    // // Set the output content descriptor on the final processor.
    // if ((p.setContentDescriptor(cd)) == null) {
    // throw new MediaException();
    // }
    //      
    // // We are done with programming the processor. Let's just
    // // realize and prefetch it.
    // if (!waitForState(p, Processor.Prefetched)) {
    // throw new MediaException("Failed to realize the processor.");
    // }
    //      
    // // String name = "file:" + System.getProperty("user.dir") +
    // File.separator + "split" + suffix + ds.idx + ext;
    // String name = "file:" + filename;
    // MediaLocator oml;
    //      
    // if ((oml = createMediaLocator(name)) == null) {
    // throw new MediaException("Cannot build media locator from: " +
    // name);
    // }
    //      
    // // Now, we'll need to create a DataSink.
    // if ((dsink = createDataSink(p, oml)) == null) {
    // throw new MediaException("Failed to create a DataSink for the
    // given output MediaLocator: " + oml);
    // }
    //      
    // dsink.addDataSinkListener(this);
    //      
    // // OK, we can now start the actual concatenation.
    // p.start();
    // dsink.start();
    //      
    // return true;
    // }
    //    
    //    
    // /**
    // * Controller Listener.
    // */
    // public void controllerUpdate(ControllerEvent evt) {
    //      
    // if (evt instanceof ControllerErrorEvent) {
    // }
    // else if (evt instanceof EndOfMediaEvent) {
    // evt.getSourceController().close();
    // }
    // }
    //    
    //    
    // /**
    // * Event handler for the file writer.
    // */
    // public void dataSinkUpdate(DataSinkEvent evt) {
    //      
    // if (evt instanceof EndOfStreamEvent ||
    // evt instanceof DataSinkErrorEvent) {
    //        
    // // Cleanup.
    // try {
    // dsink.close();
    // } catch (Exception e) {}
    // p.removeControllerListener(this);
    // ds.done = true;
    // doneFile();
    // }
    // }
    //    
    //    
    // /**
    // * Create the DataSink.
    // */
    // DataSink createDataSink(Processor p, MediaLocator outML) {
    //      
    // DataSource ds;
    //      
    // if ((ds = p.getDataOutput()) == null) {
    // return null;
    // }
    //      
    // DataSink dsink;
    //      
    // try {
    // dsink = Manager.createDataSink(ds, outML);
    // dsink.open();
    // }
    // catch (Exception e) {
    // return null;
    // }
    //      
    // return dsink;
    // }
    // }
}
