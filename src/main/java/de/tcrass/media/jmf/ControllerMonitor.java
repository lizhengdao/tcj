package de.tcrass.media.jmf;

import javax.media.Controller;
import javax.media.ControllerEvent;
import javax.media.ControllerListener;

public class ControllerMonitor implements ControllerListener {

    final static boolean WAIT_FOR_STATE     = true;
    final static boolean WAIT_FOR_NOT_STATE = true;

    private Controller   contr;
    private boolean      mode;
    private int          state;
    private Object       stateLock          = new Object();

    ControllerMonitor(Controller contr, boolean mode) {
        this.mode = mode;
        this.contr = contr;
        state = -1;
        this.contr.addControllerListener(this);
    }

    public void controllerUpdate(ControllerEvent e) {
        synchronized (stateLock) {
            state = contr.getState();
            stateLock.notifyAll();
        }
    }

    private boolean conditionSatisfied(int target) {
        return (mode ? state == target : state != target);
    }

    public boolean waitForState(int target, long maxTime) {
        boolean timeout = false;
        long t0 = System.currentTimeMillis();
        synchronized (stateLock) {
            state = contr.getState();
            while (!timeout && !conditionSatisfied(target)) {
                if (maxTime < 0) {
                    try {
                        stateLock.wait();
                    }
                    catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
                else {
                    try {
                        stateLock.wait(maxTime - (System.currentTimeMillis() - t0));
                    }
                    catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    timeout = ((System.currentTimeMillis() - t0) > maxTime);
                }
            }
        }
        return timeout;
    }

}
