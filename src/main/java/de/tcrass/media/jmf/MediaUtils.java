package de.tcrass.media.jmf;

import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.media.Controller;
import javax.media.Processor;

import com.sun.media.MimeManager;

public class MediaUtils {

    // private static Map<String, String> mimeTable;

    // static {
    // mimeTable = (Map<String, String>) MimeManager.getMimeTable();
    // if (!mimeTable.containsKey("ogg")) {
    // MimeManager.addMimeType("ogg", "audio/ogg");
    // MimeManager.commit();
    // }
    // }

    public static boolean waitForControllerState(Controller c, int state, long maxTime) {
        ControllerMonitor cm = new ControllerMonitor(c, ControllerMonitor.WAIT_FOR_STATE);
        return cm.waitForState(state, maxTime);
    }

    public static boolean waitForControllerState(Controller c, int state) {
        return waitForControllerState(c, state, -1);
    }

    public static boolean waitForControllerStateNot(Controller c, int state, long maxTime) {
        ControllerMonitor cm = new ControllerMonitor(c, ControllerMonitor.WAIT_FOR_NOT_STATE);
        return cm.waitForState(state, maxTime);
    }

    public static boolean waitForControllerStateNot(Controller c, int state) {
        return waitForControllerStateNot(c, state, -1);
    }

    public static void configure(Processor p) {
        p.configure();
        waitForControllerState(p, Processor.Configured);
    }

    public static void realize(Processor p) {
        p.realize();
        waitForControllerState(p, Controller.Realized);
    }

    private static String[] getFileTypesForMimeClass(String mimeClass) {
        List<String> extList = new Vector<String>();
        Map<String, String> mimeTable = MimeManager.getMimeTable();
        for (String ext : mimeTable.keySet()) {
            String type = mimeTable.get(ext);
            if (type.startsWith(mimeClass)) {
                extList.add(ext);
            }
        }
        return extList.toArray(new String[] {});
    }

    public static String[] getAudioFileTypes() {
        return getFileTypesForMimeClass("audio");
    }

    public static String[] getVideoFileTypes() {
        return getFileTypesForMimeClass("video");
    }

}
