package de.tcrass.media.jmf;

import de.tcrass.util.SysUtils;

public class AudioSettings extends MediaSettings {

    private int steps = 0;

    public int getRemainingFadeSteps() {
        return steps;
    }

    public void setRemainingFadeSteps(int steps) {
        this.steps = steps;
    }

    // --- Implementation of Cloneable

    @Override
    public Object clone() {
        Object o = null;
        try {
            AudioSettings a = (AudioSettings) super.clone();
            if (a != null) {
                a.steps = steps;
            }
            o = a;
        }
        catch (Exception e) {
            SysUtils.doNothing();
        }
        return o;
    }

}
