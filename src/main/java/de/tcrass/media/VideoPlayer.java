package de.tcrass.media;

import java.awt.Component;


public interface VideoPlayer extends MediaPlayer {

    public Component getComponent();
    
}
