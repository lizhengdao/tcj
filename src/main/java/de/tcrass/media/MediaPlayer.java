package de.tcrass.media;

import java.io.File;
import java.net.URI;

import org.gstreamer.State;


public interface MediaPlayer {

    public static final double DEFAULT_GAIN = 0.75;
    
    public void setSource(URI src);
    public void setSource(File src);
    public void setSource(String src);
    public void play();
    public void play(URI src);
    public void play(File src);
    public void play(String src);
    public void pause();
    public void stop();
    public State getState();
    public void seekPos(long pos);
    public long getPos();
    public void rewind();
    public void setGain(double gain);
    public double getGain();
    public void setAutoRepeat(boolean loop);
    public boolean isAutoRepeat();
    public void dispose();
    
}
