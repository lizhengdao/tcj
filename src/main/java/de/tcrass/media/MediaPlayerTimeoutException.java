package de.tcrass.media;

public class MediaPlayerTimeoutException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private int               target;
    private int               timeout;

    public MediaPlayerTimeoutException(int ts, int to) {
        target = ts;
        timeout = to;
    }

    @Override
    public String toString() {
        return "Media player didn't reach state " + target + " after " + timeout + " ms.";
    }

}
