package de.tcrass.sql;

import java.util.List;

public interface ORDBHelper {

    public void setEasySQL(EasySQL esql);

    public List<String> getParentTables(String tableName);

    public List<String> getChildTables(String tableName);

    public boolean isPossiblePrimaryKey(ORColumn col);
    
    public boolean areCompatibleColumns(ORColumn col1, ORColumn col2);
    
}
