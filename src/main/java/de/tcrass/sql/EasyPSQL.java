package de.tcrass.sql;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import de.tcrass.util.SysUtils;

public class EasyPSQL extends EasySQL {

    public static final String PSQL_DRIVER_CLASSNAME = "org.postgresql.Driver";

    static {
        try {
            Class.forName(PSQL_DRIVER_CLASSNAME);
        }
        catch (ClassNotFoundException e) {
            SysUtils.reportException(e,
                "PostgreSQL JDBC driver not installed or not in classpath?");
        }
    }

    public EasyPSQL(String confname) throws IOException {
        super(confname);
    }

    public EasyPSQL(File conffile) throws IOException {
        super(conffile);
    }

    public EasyPSQL(Properties p) {
        super(p);
    }

    public EasyPSQL(String url, String user, String password) {
        super(PSQL_DRIVER_CLASSNAME, url, user, password);
    }

    public EasyPSQL(String url, String user, String password, Properties prop) {
        super(PSQL_DRIVER_CLASSNAME, url, user, password, prop);
    }

    public EasyPSQL(String url, Properties p) {
        super(PSQL_DRIVER_CLASSNAME, url, p);
    }

    @Override
    protected void init(String driver, String url, Properties p) {
        if ((driver == null) || driver.equals("")) {
            driver = PSQL_DRIVER_CLASSNAME;
        }
        super.init(driver, url, p);
    }

}
