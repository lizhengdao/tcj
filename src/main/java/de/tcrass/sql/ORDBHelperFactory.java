package de.tcrass.sql;

public class ORDBHelperFactory {

    public static ORDBHelper createFor(EasySQL esql) {
        ORDBHelper ir;
        if (EasyPSQL.PSQL_DRIVER_CLASSNAME.equals(esql.getDriverName())) {
            ir = new PSQLDBHelper();
        }
        else {
            ir = new DefaultORDBHelper();
        }
        ir.setEasySQL(esql);
        return ir;
    }
}
