package de.tcrass.io;

public class MissingValueForArgumentException extends ArgumentsParseException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public MissingValueForArgumentException(String arg) {
        super("Missing value for argument: " + arg);
    }

}
