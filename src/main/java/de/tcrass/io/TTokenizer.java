package de.tcrass.io;

import java.io.Reader;
import java.io.StreamTokenizer;

public class TTokenizer extends StreamTokenizer {

    private char quoteChar;

    public TTokenizer(Reader r) {
        super(r);
    }

    public void dontParseNumbers() {
        ordinaryChars('0', '9');
        ordinaryChar('-');
        ordinaryChar('.');
        wordChars('0', '9');
    }

    public void quoteChar(char quoteChar) {
        this.quoteChar = quoteChar;
        super.quoteChar(quoteChar);
    }

    public boolean wasEof() {
        return (ttype == StreamTokenizer.TT_EOF);
    }

    public boolean wasEol() {
        return (ttype == StreamTokenizer.TT_EOL);
    }

    public boolean wasChar() {
        return ((ttype >= 0) && (ttype != quoteChar));
    }

    public char getChar() {
        return (wasChar() ? getChar() : 0);
    }

    public String getCharStr() {
        return Character.toString((char) ttype);
    }

    public boolean charIn(char[] chars) {
        boolean found = false;
        if (wasChar()) {
            for (char element : chars) {
                if (ttype == element) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    }

    public boolean wasNumber() {
        return (ttype == StreamTokenizer.TT_NUMBER);
    }

    public double getNumber() {
        double number = Double.NaN;
        if (wasNumber()) {
            number = nval;
        }
        return number;
    }

    public int getInt() {
        int n = 0;
        if (wasNumber()) {
            n = (int) getNumber();
        }
        return n;
    }

    public double getFloat() {
        return getNumber();
    }

    public boolean wasWord() {
        return (ttype == StreamTokenizer.TT_WORD);
    }

    public String getWord() {
        String word = "";
        if (wasWord()) {
            word = sval;
        }
        return word;
    }

    public boolean wordIn(String[] words) {
        boolean found = false;
        if (wasWord()) {
            String word = getWord();
            for (String element : words) {
                if (word.equals(element)) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    }

    public String getString() {
        String s = "";
        if (wasChar()) {
            s = getCharStr();
        }
        else if (wasNumber()) {
            s = Double.toString(getNumber());
        }
        else if (wasQuote()) {
            s = getQuote();
        }
        else if (wasWord()) {
            s = getWord();
        }
        return s;
    }

    public boolean stringIn(String[] strings) {
        boolean found = false;
        String string = getString();
        for (String element : strings) {
            if (string.equals(element)) {
                found = true;
                break;
            }
        }
        return found;
    }

    public boolean wasQuote() {
        return (ttype == quoteChar);
    }

    public String getQuote() {
        String s = "";
        if (wasQuote()) {
            s = sval;
        }
        return s;
    }

}
