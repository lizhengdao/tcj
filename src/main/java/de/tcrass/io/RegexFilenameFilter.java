package de.tcrass.io;

import java.io.File;
import java.io.FilenameFilter;

public class RegexFilenameFilter implements FilenameFilter {

    private String  pattern;
    private boolean caseInsensitive = true;

    public RegexFilenameFilter(String pattern) {
        this.pattern = pattern;
    }

    public RegexFilenameFilter(String pattern, boolean caseInsensitive) {
        this.pattern = pattern;
        this.caseInsensitive = caseInsensitive;
    }

    public boolean accept(File dir, String name) {
        String fileName;
        if (caseInsensitive) {
            fileName = name.toLowerCase();
        }
        else {
            fileName = name;
        }
        return fileName.matches(pattern);
    }

}
