package de.tcrass.io.dumper;


public interface FieldVisitor {

    public boolean canRestore();

    public Object createDumpContainer(Object o);
    
    public void beginObject(Object dump, Object o);
    
    public void dumpField(Object dump, Object o);

    public void endObject(Object dump, Object o);

}
