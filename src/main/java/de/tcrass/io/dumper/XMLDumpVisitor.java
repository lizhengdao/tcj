package de.tcrass.io.dumper;

import java.util.Stack;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.tcrass.xml.XMLUtils;


public class XMLDumpVisitor implements FieldVisitor {

    private Document d;
    private Stack<Element> elements;
    
    public XMLDumpVisitor() {
        elements = new Stack<Element>();
    }
    
    public boolean canRestore() {
        return true;
    }
    
    public Object createDumpContainer(Object o) {
        d = XMLUtils.createDocument(); 
        return d;
    }
    
    public void beginObject(Object dump, Object o) {
        Document d = (Document)dump;
        Element e = d.createElement("object");
        XMLUtils.setAttribute(e, "class", o.getClass().getCanonicalName());
        if (elements.empty()) {
            d.appendChild(e);
        }
        else {
            elements.peek().appendChild(e);
        }
        elements.push(e);
    }

    public void dumpField(Object dump, Object o) {
        
    }
    
    public void endObject(Object dump, Object o) {
        elements.pop();
    }
    
}
