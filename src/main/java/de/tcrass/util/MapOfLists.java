package de.tcrass.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class MapOfLists<K, V> extends LinkedHashMap<K, List<V>> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public List<V> getList(K key) {
        if (get(key) == null) {
            put(key, new ArrayList<V>());
        }
        return get(key);
    }

    public void add(K key, V val) {
        getList(key).add(val);
    }

    public void remove(K key, V val) {
        getList(key).remove(val);
    }

    public List<List<V>> getLists() {
        return new ArrayList<List<V>>(values());
    }

    public List<V> getAllElements() {
        ArrayList<V> v = new ArrayList<V>();
        List<List<V>> lists = getLists();
        for (List<V> u : lists) {
            v.addAll(u);
        }
        return v;
    }

}
