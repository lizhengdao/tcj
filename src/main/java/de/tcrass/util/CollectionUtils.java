package de.tcrass.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class CollectionUtils {

    // --- Private methods ---

    private static String arrayElementsToString(Object[] a) {
        StringBuffer s = new StringBuffer();
        int i = 0;
        while (i < a.length) {
            s.append((a[i].toString()));
            i++;
            if (i < a.length) {
                s.append(", ");
            }
        }
        return s.toString();
    }

    // --- Public methods ---

    // --- Arrays

    public static String arrayToString(Object[] a) {
        return "(" + arrayElementsToString(a) + ")";
    }

    public static String arrayToString(boolean[] a) {
        Object[] r = new Object[a.length];
        for (int i = 0; i < a.length; i++) {
            r[i] = Boolean.valueOf(a[i]);
        }
        return arrayToString(r);
    }

    public static String arrayToString(byte[] a) {
        Object[] r = new Object[a.length];
        for (int i = 0; i < a.length; i++) {
            r[i] = Byte.valueOf(a[i]);
        }
        return arrayToString(r);
    }

    public static String arrayToString(short[] a) {
        Object[] r = new Object[a.length];
        for (int i = 0; i < a.length; i++) {
            r[i] = Short.valueOf(a[i]);
        }
        return arrayToString(r);
    }

    public static String arrayToString(int[] a) {
        Object[] r = new Object[a.length];
        for (int i = 0; i < a.length; i++) {
            r[i] = Integer.valueOf(a[i]);
        }
        return arrayToString(r);
    }

    public static String arrayToString(long[] a) {
        Object[] r = new Object[a.length];
        for (int i = 0; i < a.length; i++) {
            r[i] = Long.valueOf(a[i]);
        }
        return arrayToString(r);
    }

    public static String arrayToString(float[] a) {
        Object[] r = new Object[a.length];
        for (int i = 0; i < a.length; i++) {
            r[i] = Float.valueOf(a[i]);
        }
        return arrayToString(r);
    }

    public static String arrayToString(double[] a) {
        Object[] r = new Object[a.length];
        for (int i = 0; i < a.length; i++) {
            r[i] = Double.valueOf(a[i]);
        }
        return arrayToString(r);
    }

    public static int indexOf(Object[] array, Object elem) {
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (elem != null) {
                if (elem.equals(array[i])) {
                    index = i;
                    break;
                }
            }
            else if (array[i] == null) {
                index = i;
                break;
            }
        }
        return index;
    }

    // --- Lists

    public static <T> void sortList(List<T> l, Comparator<? super T> c) {
        Object[] a = l.toArray();
        Arrays.sort(a, (Comparator) c);
        l.clear();
        for (Object element : a) {
            l.add((T) element);
        }
    }

    public static String listToString(Collection<?> l) {
        return "[" + arrayElementsToString(l.toArray()) + "]";
    }

    // --- Maps

    public static <K, V> void mergeOverwrite(Map<K, V> p, Map<? extends K, ? extends V> add) {
        for (K key : add.keySet()) {
            p.put(key, add.get(key));
        }
    }

    public static <K, V> void mergeAdd(Map<K, V> p, Map<? extends K, ? extends V> add) {
        for (K key : add.keySet()) {
            if (!p.containsKey(key)) {
                p.put(key, add.get(key));
            }
        }
    }

    public static <K, V> void mergeIntersect(Map<K, V> p, Map<? extends K, ? extends V> add) {
        for (K key : add.keySet()) {
            if (p.containsKey(key)) {
                p.put(key, add.get(key));
            }
        }
    }

    public static String mapToString(Map<?, ?> m) {
        StringBuffer s = new StringBuffer("{");
        Object[] keys = m.keySet().toArray();
        for (int i = 0; i < keys.length; i++) {
            s.append(keys[i].toString() + " => " + m.get(keys[i]).toString());
            if (i < (keys.length - 1)) {
                s.append(", ");
            }
        }
        s.append("}");
        return s.toString();
    }

    // --- Configuration handling

    public static String getConfName(String name) {
        String file = name;
        String host = SysUtils.getLocalHostName();
        if (host != null) {
            file = name + "@" + host;
        }
        return file;
    }

    public static void storeHostProperties(Properties p, String name, String comm)
    throws IOException {
        String file = getConfName(name) + ".conf";
        p.store(new FileOutputStream(file), comm);
    }

    public static void storeHostProperties(Properties p, String name) throws IOException {
        String comm = "Configuration file for " + SysUtils.getProgramName();
        String host = SysUtils.getLocalHostName();
        if (host != null) {
            comm = comm + " at host " + host;
        }
        storeHostProperties(p, name, comm);
    }

    public static Properties loadProperties(File pfile) throws IOException {
        Properties p = new Properties();
        p.load(new FileInputStream(pfile));
        return p;
    }

    public static Properties loadProperties(String filename) throws IOException {
        return loadProperties(new File(filename));
    }

    public static Properties loadHostProperties(String name) throws IOException {
        String file = getConfName(name) + ".conf";
        Properties p = new Properties();
        p.load(new FileInputStream(file));
        return p;
    }

}
