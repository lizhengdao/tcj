package de.tcrass.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class provides static methods for conveniently converting
 * arbitrary {@link java.lang.Object}s into primitive types,
 * {@link java.lang.String}s, {@link java.util.Set}s,
 * {@link java.util.List}s or {@link java.util.Map}s. In case no
 * lossless conversion is possible, reasonable measures are undertaken
 * to return a meaningful result anyway. In particular, null values are
 * handled to prevent NullPointerExceptions from being thrown on any
 * account.
 * <p>
 * As a final fallback, all methods will convert the supplied Object to
 * a String using its {@link java.lang.Object#toString()} methods and
 * try to parse it as an Object of the appropriate type. If that fails
 * as well, a default value will be returned.
 */
public class DataTypeConverter {

    /**
     * Converts the given Object into a boolean value. If it isn't a
     * Boolean instance already, the following conversion rules are
     * applied:
     * <ul>
     * <li>null: false <li>Collection/Map: true, if the number of items
     * is greater than zero <li>Number: true, if not equal zero <li>
     * String: true, except the String equals (case-insensitive) any of
     * the values "", "-", "0", "false", "no" <li>Fallback default:
     * false
     * </ul>
     * 
     * @param o
     *            the Object to be converted
     * @return the converted Object
     */
    public static boolean toBoolean(Object o) {
        boolean x = false;
        if (o != null) {
            if (o instanceof Boolean) {
                x = (Boolean) o;
            }
            else if (o instanceof Collection) {
                x = (((Collection<?>) o).size() > 0);
            }
            else if (o instanceof Map) {
                x = (((Map<?, ?>) o).size() > 0);
            }
            else if (toDouble(o) != 0) {
                x = true;
            }
            else if (o instanceof String) {
                x = (!((String) o).equals("") && !((String) o).equals("-") &&
                     !((String) o).equals("0") && !((String) o).equalsIgnoreCase("false") && !((String) o).equalsIgnoreCase("no"));
            }
            else {
                try {
                    x = Boolean.parseBoolean(o.toString());
                }
                catch (NumberFormatException ex) {
                }
            }
        }
        return x;
    }

    /**
     * Converts the given Object into a char value. If it isn't a
     * Character instance already, the following conversion rules are
     * applied:
     * <ul>
     * <li>null: the null character <li>Number: the char value
     * corresponding to the value returned by the Number's
     * {@link java.lang.Number#intValue()} method <li>Boolean: '1' if
     * true, '0' else <li>Collection/Map: the char value corresponding
     * to the Collection's/Map's size <li>Fallback default: the null
     * character
     * </ul>
     * 
     * @param o
     *            the Object to be converted
     * @return the converted Object
     */
    public static char toChar(Object o) {
        char x = 0;
        if (o != null) {
            if (o instanceof Character) {
                x = ((Character) o).charValue();
            }
            else if (o instanceof Number) {
                x = (char) ((Number) o).intValue();
            }
            else if (o instanceof Boolean) {
                x = ((Boolean) o ? '1' : '0');
            }
            else if (o instanceof Collection) {
                x = Character.valueOf((char) ((Collection<?>) o).size());
            }
            else if (o instanceof Map) {
                x = Character.valueOf((char) ((Map<?, ?>) o).size());
            }
            else {
                try {
                    x = Character.valueOf((char) toInt(o));
                }
                catch (NumberFormatException ex) {
                }
            }
        }
        return x;
    }

    /**
     * Converts the given Object into a byte value. If it isn't a Byte
     * instance already, the following conversion rules are applied:
     * <ul>
     * <li>null: 0 <li>Number: the value returned by the Number's
     * {@link java.lang.Number#byteValue()} method <li>Character: the
     * byte value corresponding to the Characters ordinal number <li>
     * Boolean: 1 if true, 0 else <li>Collection/Map: the byte value
     * corresponding to the Collection's/Map's size <li>Fallback
     * default: 0
     * </ul>
     * 
     * @param o
     *            the Object to be converted
     * @return the converted Object
     */
    public static byte toByte(Object o) {
        byte x = 0;
        if (o != null) {
            if (o instanceof Number) {
                x = ((Number) o).byteValue();
            }
            else if (o instanceof Character) {
                x = Integer.valueOf((((Character) o).charValue())).byteValue();
            }
            else if (o instanceof Boolean) {
                x = Integer.valueOf((Boolean) o ? 1 : 0).byteValue();
            }
            else if (o instanceof Collection) {
                x = Integer.valueOf(((Collection<?>) o).size()).byteValue();
            }
            else if (o instanceof Map) {
                x = Byte.valueOf((byte) ((Map<?, ?>) o).size());
            }
            else {
                try {
                    x = Byte.parseByte(o.toString());
                }
                catch (NumberFormatException ex) {
                }
            }
        }
        return x;
    }

    /**
     * Converts the given Object into a short value. If it isn't a Short
     * instance already, the following conversion rules are applied:
     * <ul>
     * <li>null: 0 <li>Number: the value returned by the Number's
     * {@link java.lang.Number#shortValue()} method <li>Character: the
     * short value corresponding to the Characters ordinal number <li>
     * Boolean: 1 if true, 0 else <li>Collection/Map: the short value
     * corresponding to the Collection's/Map's size <li>Fallback
     * default: 0
     * </ul>
     * 
     * @param o
     *            the Object to be converted
     * @return the converted Object
     */
    public static short toShort(Object o) {
        short x = 0;
        if (o != null) {
            if (o instanceof Number) {
                x = ((Number) o).shortValue();
            }
            else if (o instanceof Character) {
                x = Integer.valueOf((((Character) o).charValue())).shortValue();
            }
            else if (o instanceof Boolean) {
                x = Integer.valueOf((Boolean) o ? 1 : 0).shortValue();
            }
            else if (o instanceof Collection) {
                x = Integer.valueOf(((Collection<?>) o).size()).shortValue();
            }
            else if (o instanceof Map) {
                x = Short.valueOf((short) ((Map<?, ?>) o).size());
            }
            else {
                try {
                    x = Short.parseShort(o.toString());
                }
                catch (NumberFormatException ex) {
                }
            }
        }
        return x;
    }

    /**
     * Converts the given Object into an int value. If it isn't a
     * Integer instance already, the following conversion rules are
     * applied:
     * <ul>
     * <li>null: 0 <li>Number: the value returned by the Number's
     * {@link java.lang.Number#intValue()} method <li>Character: the int
     * value corresponding to the Characters ordinal number <li>Boolean:
     * 1 if true, 0 else <li>Collection/Map: the int value corresponding
     * to the Collection's/Map's size <li>Fallback default: 0
     * </ul>
     * 
     * @param o
     *            the Object to be converted
     * @return the converted Object
     */
    public static int toInt(Object o) {
        int x = 0;
        if (o != null) {
            if (o instanceof Number) {
                x = ((Number) o).intValue();
            }
            else if (o instanceof Character) {
                x = ((Character) o);
            }
            else if (o instanceof Boolean) {
                x = ((Boolean) o ? 1 : 0);
            }
            else if (o instanceof Collection) {
                x = ((Collection<?>) o).size();
            }
            else if (o instanceof Map) {
                x = Integer.valueOf(((Map<?, ?>) o).size());
            }
            else {
                try {
                    x = Integer.parseInt(o.toString());
                }
                catch (NumberFormatException ex) {
                }
            }
        }
        return x;
    }

    /**
     * Converts the given Object into a long value. If it isn't a Long
     * instance already, the following conversion rules are applied:
     * <ul>
     * <li>null: 0 <li>Number: the value returned by the Number's
     * {@link java.lang.Number#longValue()} method <li>Character: the
     * long value corresponding to the Characters ordinal number <li>
     * Boolean: 1 if true, 0 else <li>Collection/Map: the long value
     * corresponding to the Collection's/Map's size <li>Fallback
     * default: 0
     * </ul>
     * 
     * @param o
     *            the Object to be converted
     * @return the converted Object
     */
    public static long toLong(Object o) {
        long x = 0;
        if (o != null) {
            if (o instanceof Number) {
                x = ((Number) o).longValue();
            }
            else if (o instanceof Character) {
                x = ((Character) o);
            }
            else if (o instanceof Boolean) {
                x = ((Boolean) o ? 1 : 0);
            }
            else if (o instanceof Collection) {
                x = ((Collection<?>) o).size();
            }
            else if (o instanceof Map) {
                x = Long.valueOf(((Map<?, ?>) o).size());
            }
            else {
                try {
                    x = Long.parseLong(o.toString());
                }
                catch (NumberFormatException ex) {
                }
            }
        }
        return x;
    }

    /**
     * Converts the given Object into a float value. If it isn't a Float
     * instance already, the following conversion rules are applied:
     * <ul>
     * <li>null: 0 <li>Number: the value returned by the Number's
     * {@link java.lang.Number#floatValue()} method <li>Character: the
     * float value corresponding to the Characters ordinal number <li>
     * Boolean: 1 if true, 0 else <li>Collection/Map: the float value
     * corresponding to the Collection's/Map's size <li>Fallback
     * default: 0
     * </ul>
     * 
     * @param o
     *            the Object to be converted
     * @return the converted Object
     */
    public static float toFloat(Object o) {
        float x = 0;
        if (o != null) {
            if (o instanceof Number) {
                x = ((Number) o).floatValue();
            }
            else if (o instanceof Character) {
                x = ((Character) o);
            }
            else if (o instanceof Boolean) {
                x = ((Boolean) o ? 1 : 0);
            }
            else if (o instanceof Collection) {
                x = ((Collection<?>) o).size();
            }
            else if (o instanceof Map) {
                x = Float.valueOf(((Map<?, ?>) o).size());
            }
            else {
                try {
                    x = Float.parseFloat(o.toString());
                }
                catch (NumberFormatException ex) {
                }
            }
        }
        return x;
    }

    /**
     * Converts the given Object into a double value. If it isn't a
     * Double instance already, the following conversion rules are
     * applied:
     * <ul>
     * <li>null: 0 <li>Number: the value returned by the Number's
     * {@link java.lang.Number#doubleValue()} method <li>Character: the
     * double value corresponding to the Characters ordinal number <li>
     * Boolean: 1 if true, 0 else <li>Collection/Map: the double value
     * corresponding to the Collection's/Map's size <li>Fallback
     * default: 0
     * </ul>
     * 
     * @param o
     *            the Object to be converted
     * @return the converted Object
     */
    public static double toDouble(Object o) {
        double x = 0;
        if (o != null) {
            if (o instanceof Number) {
                x = ((Number) o).doubleValue();
            }
            else if (o instanceof Character) {
                x = ((Character) o);
            }
            else if (o instanceof Boolean) {
                x = ((Boolean) o ? 1 : 0);
            }
            else if (o instanceof Collection) {
                x = ((Collection<?>) o).size();
            }
            else if (o instanceof Map) {
                x = Double.valueOf(((Map<?, ?>) o).size());
            }
            else {
                try {
                    x = Double.parseDouble(o.toString());
                }
                catch (NumberFormatException ex) {
                }
            }
        }
        return x;
    }

    /**
     * Converts the given Object into a String. If it isn't a String
     * instance already, the following conversion rules are applied:
     * <ul>
     * <li>null: the empty String "" <li>Collection/Map: a String
     * representation of the Collection's/Map's contents <li>Everything
     * else: the result of the Objects
     * {@link java.lang.Object#toString()} method
     * </ul>
     * 
     * @param o
     *            the Object to be converted
     * @return the converted Object
     */
    public static String toString(Object o) {
        if (o != null) {
            if (o instanceof Collection) {
                return CollectionUtils.listToString((Collection<?>) o);
            }
            else if (o instanceof Map) {
                return CollectionUtils.mapToString((Map<?, ?>) o);
            }
            else {
                return o.toString();
            }
        }
        else {
            return "";
        }
    }

    /**
     * Converts the given Object into a Set. If it isn't a Set instance
     * already, the following conversion rules are applied:
     * <ul>
     * <li>null: an empty Set <li>Collections: a Set containing single
     * instances (according to {@link java.lang.Object#equals(Object)}
     * of the Collection's items <li>Maps: the Map's key Set <li>
     * Everything else: a Set containing the Object as sole element
     * </ul>
     * 
     * @param o
     *            the Object to be converted
     * @return the converted Object
     */
    public static Set<?> toSet(Object o) {
        Set<Object> x = new HashSet<Object>();
        if (o != null) {
            if (o instanceof Set) {
                return (Set<?>) o;
            }
            else if (o instanceof Collection) {
                x.addAll((Collection<?>) o);
            }
            else if (o instanceof Map) {
                x.addAll(((Map<?, ?>) o).keySet());
            }
            else {
                x.add(o);
            }
        }
        return x;
    }

    /**
     * Converts the given Object into a List. If it isn't a List
     * instance already, the following conversion rules are applied:
     * <ul>
     * <li>null: an empty List <li>Collections: a List containing all
     * the Collection's items <li>Maps: the Map's value Collection <li>
     * Everything else: a List containing the Object as sole element
     * </ul>
     * 
     * @param o
     *            the Object to be converted
     * @return the converted Object
     */
    public static List<?> toList(Object o) {
        List<Object> x = new ArrayList<Object>();
        if (o != null) {
            if (o instanceof List) {
                return (List<?>) o;
            }
            else if (o instanceof Collection) {
                x.addAll((Collection<?>) o);
            }
            else if (o instanceof Map) {
                x.addAll(((Map<?, ?>) o).values());
            }
            else {
                x.add(o);
            }
        }
        return x;
    }

    /**
     * Converts the given Object into a Map. If it isn't a Map instance
     * already, the following conversion rules are applied:
     * <ul>
     * <li>null: an empty List <li>Collections: a Map with the
     * Collection's items at even indides as keys and the immediately
     * succeeding items (obviously having odd indices) as corresponding
     * values <li>Everything else: a Map containing the Object as sole
     * key, associated with a null value
     * </ul>
     * 
     * @param o
     *            the Object to be converted
     * @return the converted Object
     */
    public static Map<?, ?> toMap(Object o) {
        Map<Object, Object> x = new HashMap<Object, Object>();
        if (o != null) {
            if (o instanceof Map) {
                return (Map<?, ?>) o;
            }
            else if (o instanceof Collection) {
                Iterator<?> i = ((Collection<?>) o).iterator();
                while (i.hasNext()) {
                    Object key = i.next();
                    Object val = null;
                    if (i.hasNext()) {
                        val = i.next();
                    }
                    x.put(key, val);
                }
            }
            else {
                x.put(o, null);
            }
        }
        return x;
    }

}
