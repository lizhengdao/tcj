package de.tcrass.util;

import java.util.ArrayList;
import java.util.List;

public class PositionList<E> {

    private List<Long> positions;
    private List<E>    objects;
    private long       maxPos = Long.MAX_VALUE;
    private long       minPos = Long.MIN_VALUE;

    public PositionList() {
        positions = new ArrayList<Long>();
        objects = new ArrayList<E>();
    }

    private int findIndexForPositionGE(long pos, int ia, int ib) {
        int ic;
        if (ia == ib) {
            ic = ia;
        }
        else {
            ic = ia + (ib - ia) / 2;
            long posi;
            if (ic < 0) {
                ic = 0;
            }
            else {
                posi = positions.get(ic);
                if (pos > posi) {
                    ic = findIndexForPositionGE(pos, ic + 1, ib);
                }
                else {
                    ic = findIndexForPositionGE(pos, ia, ic);
                }
            }
        }
        return ic;
    }

    private int findIndexForPositionLE(long pos, int ia, int ib) {
        int ic;
        if (ia == ib) {
            ic = ia;
        }
        else {
            ic = ia + (ib - ia + 1) / 2;
            long posi;
            if (ic < 0) {
            }
            else {
                posi = positions.get(ic);
                if (pos >= posi) {
                    ic = findIndexForPositionLE(pos, ic, ib);
                }
                else {
                    ic = findIndexForPositionLE(pos, ia, ic - 1);
                }
            }
        }
        return ic;
    }

    public int findIndexForPositionGE(long pos) {
        int idx = findIndexForPositionGE(pos, 0, positions.size());
        return idx;
    }

    public int findIndexForPositionLE(long pos) {
        int idx = findIndexForPositionLE(pos, -1, positions.size() - 1);
        return idx;
    }

    public void add(E o, long pos) {
        int last = findIndexForPositionLE(pos) + 1;
        positions.add(last, new Long(pos));
        objects.add(last, o);
        if (pos < minPos) {
            minPos = pos;
        }
        if (pos > maxPos) {
            maxPos = pos;
        }
    }

    public int size() {
        return positions.size();
    }

    public List<E> elementsInRange(long lower, long upper) {
        int ia = findIndexForPositionGE(lower);
        int ib = findIndexForPositionLE(upper);
        if (ia > ib) {
            return new ArrayList<E>();
        }
        else {
            return objects.subList(ia, ib + 1);
        }
    }

    public List<E> elementsAt(long pos) {
        return elementsInRange(pos, pos);
    }

    public List<E> elementsGE(long lower) {
        return elementsInRange(lower, maxPos);
    }

    public List<E> elementsLE(long upper) {
        return elementsInRange(minPos, upper);
    }

    public long positionAtIndex(int i) {
        return (positions.get(i)).longValue();
    }

    public E elementAtIndex(int i) {
        return objects.get(i);
    }

    public void remove(E o) {
        int index = objects.indexOf(o);
        if (index >= 0) {
            objects.remove(index);
            positions.remove(index);
        }
    }

    public void remove(int i) {
        objects.remove(i);
        positions.remove(i);
    }

    public List<Long> getPositions() {
        return positions;
    }

    public List<E> getElements() {
        return objects;
    }

}
